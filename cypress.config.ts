import { defineConfig } from 'cypress'

export default defineConfig({
    e2e: {
        baseUrl: 'http://localhost:3100',
        env: {
            smtpHost: 'localhost',
            instanceName: 'localhost',
        },
        viewportWidth: 1280,
        viewportHeight: 720,
        numTestsKeptInMemory: 250,
        trashAssetsBeforeRuns: true,
        video: false,
        chromeWebSecurity: false,
        specPattern: 'cypress/**/*.spec.ts',
    },
})
