import { Prisma } from '@prisma/client'

const servicesWithRelations = Prisma.validator<Prisma.ServiceEntryArgs>()({
    include: {
        location: true,
        user: true,
        household: true,
        service_type: true,
    },
})

export type ServiceWithRelations = Prisma.ServiceEntryGetPayload<
    typeof servicesWithRelations
>
