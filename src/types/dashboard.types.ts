import {
    User,
    ServiceType,
    Recipient,
    Program,
    Household,
    Location,
} from '@prisma/client'

export interface DashboardGetAllResponse {
    users: User[]
    service_types: ServiceType[]
    recipients: Recipient[]
    programs: Program[]
    locations: Location[]
    households: Household[]
}
