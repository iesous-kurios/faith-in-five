import { Prisma } from '@prisma/client'

const locationWithServicesProvided = Prisma.validator<Prisma.LocationArgs>()({
    include: {
        service_entries: {
            include: {
                household: true,
                service_type: true,
                user: true,
                location: true,
                recipient: true,
            },
        },
    },
})

export type LocationWithServicesProvided = Prisma.LocationGetPayload<
    typeof locationWithServicesProvided
>
