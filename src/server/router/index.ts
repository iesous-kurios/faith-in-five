import { createRouter } from './context'

import superjson from 'superjson'

import { userRoutes } from './user.routes'
import { serviceRoutes } from './service.routes'
import { programRoutes } from './program.routes'
import { locationRoutes } from './location.routes'
import { dashboardRoutes } from './dashboard.routes'
import { householdRoutes } from './household.routes'
import { recipientRoutes } from './recipient.routes'

export const appRouter = createRouter()
    .transformer(superjson)
    .merge('users.', userRoutes)
    .merge('programs.', programRoutes)
    .merge('services.', serviceRoutes)
    .merge('locations.', locationRoutes)
    .merge('households.', householdRoutes)
    .merge('recipients.', recipientRoutes)
    .merge('dashboard.', dashboardRoutes)

// export type definition of API
export type AppRouter = typeof appRouter
