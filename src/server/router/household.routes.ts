import { createProtectedRouter } from './context'

import { UpsertHouseholdSchema } from '@Schema/household.schema'

import { z } from 'zod'

export const householdRoutes = createProtectedRouter()
    .query('all', {
        resolve({ ctx }) {
            return ctx.prisma.household.findMany({
                include: { Location: true },
            })
        },
    })
    .mutation('upsert', {
        input: UpsertHouseholdSchema,
        resolve({ ctx, input }) {
            const { id, ...household } = input

            return ctx.prisma.household.upsert({
                where: {
                    id: id,
                },
                update: { id, ...household },
                create: household,
            })
        },
    })
    .mutation('delete', {
        input: z.object({ householdId: z.number() }),
        resolve({ ctx, input }) {
            return ctx.prisma.household.delete({
                where: { id: input.householdId },
            })
        },
    })
