import { createProtectedRouter } from './context'

import { UpsertRecipientSchema } from '@Schema/recipient.schema'

import { z } from 'zod'

export const recipientRoutes = createProtectedRouter()
    .query('all', {
        resolve({ ctx }) {
            return ctx.prisma.recipient.findMany({
                include: { households: true },
            })
        },
    })
    .mutation('upsert', {
        input: UpsertRecipientSchema,
        resolve({ ctx, input }) {
            const { id, ...household } = input

            return ctx.prisma.recipient.upsert({
                where: {
                    id: id,
                },
                update: { id, ...household },
                create: household,
            })
        },
    })
    .mutation('delete', {
        input: z.object({ recipientId: z.number() }),
        resolve({ ctx, input }) {
            return ctx.prisma.recipient.delete({
                where: { id: input.recipientId },
            })
        },
    })
