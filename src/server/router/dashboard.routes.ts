import { createProtectedRouter } from './context'

export const dashboardRoutes = createProtectedRouter().query('all', {
    async resolve({ ctx }) {
        const recipients = await ctx.prisma.recipient.findMany()
        const users = await ctx.prisma.user.findMany()
        const service_types = await ctx.prisma.serviceType.findMany()
        const programs = await ctx.prisma.program.findMany()
        const locations = await ctx.prisma.location.findMany()
        const households = await ctx.prisma.household.findMany()

        return {
            recipients,
            users,
            service_types,
            programs,
            locations,
            households,
        }
    },
})
