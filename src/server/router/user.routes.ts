import { createProtectedRouter } from './context'

import {
    UpsertProfileSchema,
    UpdateProfileSchema,
} from '@Schema/profile.schema'
import { z } from 'zod'

export const userRoutes = createProtectedRouter()
    .query('current', {
        resolve({ ctx }) {
            return ctx.prisma.user.findUnique({
                where: { id: ctx.session.user.id },
            })
        },
    })
    .mutation('current.update', {
        input: UpdateProfileSchema,
        resolve({ ctx, input }) {
            return ctx.prisma.user.update({
                where: { id: ctx.session.user.id },
                data: {
                    first_name: input.first_name,
                    last_name: input.last_name,
                    phone_number: input.phone_number,
                },
            })
        },
    })
    .query('all', {
        resolve({ ctx }) {
            return ctx.prisma.user.findMany()
        },
    })
    .mutation('upsert', {
        input: UpsertProfileSchema,
        resolve({ ctx, input }) {
            const { id, ...user } = input

            return ctx.prisma.user.upsert({
                where: {
                    id: id,
                },
                update: { ...user, id },
                create: user,
            })
        },
    })
    .mutation('delete', {
        input: z.object({ userId: z.string() }),
        resolve({ ctx, input: { userId } }) {
            return ctx.prisma.user.delete({ where: { id: userId } })
        },
    })
