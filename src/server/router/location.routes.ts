import { createProtectedRouter } from './context'

import { ServiceEntriesFilterParams } from '@Schema/service.schema'
import { UpsertLocationSchema } from '@Schema/location.schema'
import { z } from 'zod'

export const locationRoutes = createProtectedRouter()
    .query('map.all', {
        input: ServiceEntriesFilterParams,
        async resolve({ ctx, input }) {
            let locations = await ctx.prisma.location.findMany({
                include: {
                    service_entries: {
                        where: {
                            user_id:
                                input?.filters.user == null
                                    ? undefined
                                    : input.filters.user,
                            recipient_id:
                                input?.filters.recipient == null
                                    ? undefined
                                    : input.filters.recipient,
                            service_type_id:
                                input?.filters.serviceType == null
                                    ? undefined
                                    : input.filters.serviceType,
                            program_id:
                                input?.filters.program == null
                                    ? undefined
                                    : input.filters.program,
                        },
                        include: {
                            household: true,
                            user: true,
                            service_type: true,
                            recipient: true,
                            location: true,
                        },
                    },
                },
            })

            locations = locations.filter(
                (loc) => loc.service_entries.length > 0
            )

            return locations
        },
    })
    .query('all', {
        resolve({ ctx }) {
            return ctx.prisma.location.findMany()
        },
    })
    .mutation('upsert', {
        input: UpsertLocationSchema,
        resolve({ ctx, input }) {
            const { id, ...location } = input

            return ctx.prisma.location.upsert({
                where: {
                    id: id,
                },
                update: { id, ...location },
                create: location,
            })
        },
    })
    .mutation('delete', {
        input: z.object({ locationId: z.number() }),
        resolve({ ctx, input }) {
            return ctx.prisma.location.delete({
                where: { id: input.locationId },
            })
        },
    })
