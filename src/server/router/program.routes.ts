import { createProtectedRouter } from './context'

import { z } from 'zod'

import { UpsertProgramSchema } from '@Schema/program.schema'

export const programRoutes = createProtectedRouter()
    .query('all', {
        resolve({ ctx }) {
            return ctx.prisma.program.findMany()
        },
    })
    .mutation('upsert', {
        input: UpsertProgramSchema,
        resolve({ ctx, input }) {
            const { id, ...program } = input

            return ctx.prisma.program.upsert({
                where: {
                    id: id,
                },
                update: { id, ...program },
                create: program,
            })
        },
    })
    .mutation('delete', {
        input: z.object({ programId: z.number() }),
        resolve({ ctx, input: { programId } }) {
            return ctx.prisma.program.delete({ where: { id: programId } })
        },
    })
