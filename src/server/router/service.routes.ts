import { createProtectedRouter } from './context'

import { UpsertServiceSchema } from '@Schema/service.schema'

import { z } from 'zod'

export const serviceRoutes = createProtectedRouter()
    .query('all', {
        async resolve({ ctx }) {
            return await ctx.prisma.serviceEntry.findMany({
                include: {
                    recipient: true,
                    household: true,
                    location: true,
                    service_type: true,
                    program: true,
                },
            })
        },
    })
    .mutation('upsert', {
        input: UpsertServiceSchema,
        resolve({ ctx, input }) {
            const { id, ...service } = input

            return ctx.prisma.serviceEntry.upsert({
                where: {
                    id: id,
                },
                update: { id, ...service },
                create: service,
            })
        },
    })
    .mutation('delete', {
        input: z.object({ serviceId: z.number() }),
        resolve({ ctx, input }) {
            return ctx.prisma.serviceEntry.delete({
                where: { id: input.serviceId },
            })
        },
    })
