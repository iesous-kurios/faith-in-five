import { trpc } from 'src/utils/trpc'

import { useQueryClient } from 'react-query'

export const useAllUsers = () => {
    return trpc.useQuery(['users.all'])
}

export const useUpsertUser = () => {
    const queryClient = useQueryClient()

    return trpc.useMutation(['users.upsert'], {
        onSuccess: () => queryClient.invalidateQueries('users.all'),
    })
}

export const useCurrentUser = () => {
    return trpc.useQuery(['users.current'])
}

export const useUpdateCurrentUser = () => {
    return trpc.useMutation(['users.current.update'])
}

export const useDeleteUser = () => {
    return trpc.useMutation(['users.delete'])
}
