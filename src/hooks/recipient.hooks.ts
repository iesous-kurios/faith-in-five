import { trpc } from 'src/utils/trpc'

import { useQueryClient } from 'react-query'

export const useRecipients = () => {
    return trpc.useQuery(['recipients.all'])
}

export const useUpsertRecipient = () => {
    const queryClient = useQueryClient()

    return trpc.useMutation(['recipients.upsert'], {
        onSuccess: () => queryClient.invalidateQueries('recipients.all'),
    })
}

export const useDeleteRecipient = () => {
    return trpc.useMutation(['recipients.delete'])
}
