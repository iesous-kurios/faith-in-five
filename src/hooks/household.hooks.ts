import { trpc } from 'src/utils/trpc'

import { useQueryClient } from 'react-query'

export const useHouseholds = () => {
    return trpc.useQuery(['households.all'])
}

export const useUpsertHousehold = () => {
    const queryClient = useQueryClient()

    return trpc.useMutation(['households.upsert'], {
        onSuccess: () => queryClient.invalidateQueries('households.all'),
    })
}

export const useDeleteHousehold = () => {
    return trpc.useMutation(['households.delete'])
}
