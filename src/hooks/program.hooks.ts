import { trpc } from 'src/utils/trpc'

export const useAllPrograms = () => {
    return trpc.useQuery(['programs.all'])
}

export const useUpsertProgram = () => {
    return trpc.useMutation(['programs.upsert'])
}

export const useDeleteProgram = () => {
    return trpc.useMutation(['programs.delete'])
}
