import { trpc } from 'src/utils/trpc'

import { useQueryClient } from 'react-query'

export const useServices = () => {
    return trpc.useQuery(['services.all'])
}

export const useUpsertService = () => {
    const queryClient = useQueryClient()

    return trpc.useMutation(['services.upsert'], {
        onSuccess: () => queryClient.invalidateQueries('services.all'),
    })
}

export const useDeleteService = () => {
    return trpc.useMutation(['services.delete'])
}
