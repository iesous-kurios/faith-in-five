import { trpc } from 'src/utils/trpc'

export const useLocationsWithServices = () => {
    return trpc.useQuery(['locations.map.all'])
}

export const useLocations = () => {
    return trpc.useQuery(['locations.all'])
}

export const useUpsertLocation = () => {
    return trpc.useMutation(['locations.upsert'])
}

export const useDeleteLocation = () => {
    return trpc.useMutation(['locations.delete'])
}
