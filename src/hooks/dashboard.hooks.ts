import { trpc } from 'src/utils/trpc'

export const useDashboard = () => {
    return trpc.useQuery(['dashboard.all'])
}
