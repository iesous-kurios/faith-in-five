import type { NextPage } from 'next'

import { useLocationsWithServices } from 'src/hooks/location.hooks'

import { useDashboard } from 'src/hooks/dashboard.hooks'

import { ServiceMap, ServiceMapFilters } from '@Components/Dashboard'

import DashboardStatistics from '@Components/Dashboard/DashboardStatistics/DashboardStatistics'

import { Loading } from '@Components/Partials/Loading'

const Dashboard: NextPage = () => {
    const {
        data: dashboard,
        isLoading: isDashboardLoading,
        isError: isDashboardError,
        error: dashboardError,
    } = useDashboard()

    const {
        data: locations,
        isLoading: isLocationsLoading,
        isError: isLocationsError,
        error: locationError,
    } = useLocationsWithServices()

    const isLoading = isLocationsLoading || isDashboardLoading
    const isError = isLocationsError || isDashboardError

    if (isLoading) {
        return (
            <div className={'grid h-screen place-items-center'}>
                <Loading />
            </div>
        )
    }

    if (isError) {
        return (
            <h1 className={'grid h-screen place-items-center'}>
                {locationError?.message || dashboardError?.message}
            </h1>
        )
    }

    return (
        <div className={'flex flex-col align-center'}>
            <ServiceMapFilters dashboard={dashboard!} />
            <ServiceMap locations={locations!} />
            <DashboardStatistics />
        </div>
    )
}

export default Dashboard
