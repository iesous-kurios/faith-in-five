import { NextPage } from 'next'

import { EmployeesTable } from '@Components/Tables/EmployeesTable'
import { useAllUsers, useCurrentUser } from '../hooks/user.hooks'
import { User } from '@prisma/client'

import { Loading } from '@Components/Partials/Loading'

const Employees: NextPage = () => {
    const { data: users, isLoading, isError, error } = useAllUsers()

    const { data: currentUser } = useCurrentUser()

    if (isLoading) {
        return (
            <div className={'grid h-screen place-items-center'}>
                <Loading />
            </div>
        )
    }

    if (isError) {
        return <h1>{error?.message}</h1>
    }

    return (
        <div className={'mx-auto w-full py-32 px-4'}>
            <EmployeesTable
                users={users as User[]}
                currentUser={currentUser as User}
            />
        </div>
    )
}

export default Employees
