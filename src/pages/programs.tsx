import { NextPage } from 'next'

import { User, Program } from '@prisma/client'

import { ProgramsTable } from '@Components/Tables/ProgramsTable'

import { useAllPrograms } from '../hooks/program.hooks'
import { useCurrentUser } from '../hooks/user.hooks'

import { Loading } from '@Components/Partials/Loading'

const Programs: NextPage = () => {
    const { data: programs, isLoading, isError, error } = useAllPrograms()

    const { data: currentUser } = useCurrentUser()

    if (isLoading) {
        return (
            <div className={'grid h-screen place-items-center'}>
                <Loading />
            </div>
        )
    }

    if (isError) {
        return <h1>{error?.message}</h1>
    }

    return (
        <div className={'mx-auto w-full py-32 px-4'}>
            <ProgramsTable
                programs={programs as Program[]}
                currentUser={currentUser as User}
            />
        </div>
    )
}

export default Programs
