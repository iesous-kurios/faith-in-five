import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'

import { signIn, useSession } from 'next-auth/react'

import { Button } from '@mui/material'

import { useRouter } from 'next/router'

import BackgroundImage from 'public/splash.jpg'

const Home: NextPage = () => {
    const session = useSession()

    const router = useRouter()

    return (
        <>
            <Head>
                <title>Faith in Five</title>
                <meta
                    name="description"
                    content="Home for data and reports for all Faith in Five outreach events"
                />
            </Head>

            <main
                className={
                    'bg-gradient-to-tr from-blue-600 to-gray-700 w-full h-screen relative'
                }
            >
                <div
                    className={
                        'flex flex-col w-full h-full text-center pt-32 text-white font-bold'
                    }
                >
                    <div>
                        <h1 className={'text-6xl italic'}>Faith in Five</h1>
                    </div>
                    <div className={'mt-10'}>
                        <p className={'text-3xl italic '}>
                            Your way to track all Faith in Five outreach events
                        </p>
                    </div>

                    {!session.data ? (
                        <div className={'mt-10'}>
                            <Button
                                className={'w-96'}
                                style={{ color: 'white' }}
                                size={'large'}
                                onClick={() => signIn()}
                            >
                                Sign in / Register
                            </Button>
                        </div>
                    ) : (
                        <div className={'mt-10'}>
                            <Button
                                className={'w-96'}
                                style={{ color: 'white' }}
                                size={'large'}
                                onClick={() => router.replace('/dashboard')}
                            >
                                View Dashboard
                            </Button>
                        </div>
                    )}
                </div>
            </main>
        </>
    )
}

export default Home
