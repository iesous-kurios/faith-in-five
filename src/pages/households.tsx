import { NextPage } from 'next'

import { useHouseholds } from '../hooks/household.hooks'
import { HouseholdsTable } from '@Components/Tables/HouseholdsTable'

import { Loading } from '@Components/Partials/Loading'

import { useLocations } from '../hooks/location.hooks'

const Households: NextPage = () => {
    const { data: households, isLoading, isError, error } = useHouseholds()

    const {
        data: locations,
        isLoading: isLocLoading,
        isError: isLocError,
        error: locError,
    } = useLocations()

    if (isLoading || isLocLoading) {
        return (
            <div className={'grid h-screen place-items-center'}>
                <Loading />
            </div>
        )
    }

    if (isError || isLocError) {
        return <h1>{error?.message || locError?.message}</h1>
    }

    return (
        <div className={'mx-auto w-full py-32 px-4'}>
            <HouseholdsTable households={households!} locations={locations!} />
        </div>
    )
}

export default Households
