import { NextApiRequest, NextApiResponse } from 'next'

const Home = async (req: NextApiRequest, res: NextApiResponse) => {
    res.json({ api: 'up' })
}

export default Home
