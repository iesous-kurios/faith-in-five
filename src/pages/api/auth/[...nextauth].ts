import NextAuth, { type NextAuthOptions } from 'next-auth'

// Prisma adapter for NextAuth, optional and can be removed
import { PrismaAdapter } from '@next-auth/prisma-adapter'
import { prisma } from '../../../server/db/client'
import { env } from '../../../env/server.mjs'
import Email from 'next-auth/providers/email'

export const authOptions: NextAuthOptions = {
    // Include user.id on session
    callbacks: {
        session({ session, user }) {
            if (session.user) {
                session.user.id = user.id
                session.user.role = user.role
            }
            return session
        },

        signIn({ user }) {
            if (!user.name) {
                if (user.first_name || user.last_name) {
                    user.name = user.first_name + ' ' + user.last_name
                } else {
                    user.name = user?.email?.split('@')[0]
                }
            }
            return true
        },
    },
    theme: {
        colorScheme: 'light',
    },
    // Configure one or more authentication providers
    adapter: PrismaAdapter(prisma),
    providers: [
        Email({
            server: env.EMAIL_SERVER,
            from: env.EMAIL_FROM,
        }),
    ],
}

export default NextAuth(authOptions)
