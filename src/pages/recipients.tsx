import { NextPage } from 'next'

import { useRecipients } from 'src/hooks/recipient.hooks'

import { RecipientsTable } from '@Components/Tables/RecipientsTable'

import { Loading } from '@Components/Partials/Loading'
import { useHouseholds } from '../hooks/household.hooks'

const Households: NextPage = () => {
    const { data: recipients, isLoading, isError, error } = useRecipients()

    const {
        data: households,
        isLoading: isHLoading,
        isError: isHError,
        error: hError,
    } = useHouseholds()

    if (isLoading || isHLoading) {
        return (
            <div className={'grid h-screen place-items-center'}>
                <Loading />
            </div>
        )
    }

    if (isError || isHError) {
        return <h1>{error?.message}</h1>
    }

    return (
        <div className={'mx-auto w-full py-32 px-4'}>
            <RecipientsTable
                recipients={recipients!}
                households={households!}
            />
        </div>
    )
}

export default Households
