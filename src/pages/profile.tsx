import type { NextPage } from 'next'

import React from 'react'

import { useCurrentUser } from 'src/hooks/user.hooks'

import { EditProfileForm } from '@Forms/EditProfileForm'

import { Loading } from '@Components/Partials/Loading'

const Profile: NextPage = () => {
    const { data: user, isLoading, isError, error } = useCurrentUser()

    if (isLoading) {
        return (
            <div className={'grid h-screen place-items-center'}>
                <Loading />
            </div>
        )
    }

    if (isError) {
        return <p>{error?.message}</p>
    }

    return (
        <div className="bg-gray-200 min-h-screen">
            <div className="max-w-xl mx-auto w-full py-32 px-4">
                <EditProfileForm user={user!} />
            </div>
        </div>
    )
}

export default Profile
