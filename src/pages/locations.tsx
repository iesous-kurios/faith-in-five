import { NextPage } from 'next'

import { User, Location } from '@prisma/client'

import { LocationsTable } from '@Components/Tables/LocationsTable'

import { useCurrentUser } from '../hooks/user.hooks'

import { useLocations } from '../hooks/location.hooks'

import { Loading } from '@Components/Partials/Loading'

const Locations: NextPage = () => {
    const { data: locations, isLoading, isError, error } = useLocations()

    const { data: currentUser } = useCurrentUser()

    if (isLoading) {
        return (
            <div className={'grid h-screen place-items-center'}>
                <Loading />
            </div>
        )
    }

    if (isError) {
        return <h1>{error?.message}</h1>
    }

    return (
        <div className={'mx-auto w-full py-32 px-4'}>
            <LocationsTable
                locations={locations as Location[]}
                currentUser={currentUser as User}
            />
        </div>
    )
}

export default Locations
