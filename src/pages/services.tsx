import { NextPage } from 'next'

import { ServicesTable } from '@Components/Tables/ServicesTable'

import { Loading } from '@Components/Partials/Loading'

import { useServices } from 'src/hooks/service.hooks'

import { useDashboard } from '../hooks/dashboard.hooks'

const Households: NextPage = () => {
    const { data: services, isLoading, isError, error } = useServices()

    const {
        data: dashboard,
        isLoading: isDashLoading,
        isError: isDashError,
        error: dashError,
    } = useDashboard()

    if (isLoading || isDashLoading) {
        return (
            <div className={'grid h-screen place-items-center'}>
                <Loading />
            </div>
        )
    }

    if (isError || isDashError) {
        return <h1>{error?.message}</h1>
    }

    return (
        <div className={'mx-auto w-full py-32 px-4'}>
            <ServicesTable services={services!} dashboard={dashboard!} />
        </div>
    )
}

export default Households
