import { z } from 'zod'

export const UpsertProgramSchema = z.object({
    id: z.number().optional(),
    name: z.string(),
    description: z.string().nullable(),
})

export type UpsertProgramSchemaType = z.infer<typeof UpsertProgramSchema>
