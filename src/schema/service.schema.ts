import { z } from 'zod'

export const UpsertServiceSchema = z.object({
    id: z.number(),
    user_id: z.string(),
    recipient_id: z.number(),
    location_id: z.number(),
    household_id: z.number(),
    program_id: z.number(),
    service_type_id: z.number(),
    apply_service_to_household: z.boolean().default(false),
    service_date: z.date(),
    service_quantity: z.number().optional(),
    service_entry_notes: z.string().optional(),
})

export const ServiceEntriesFilterParams = z
    .object({
        filters: z.object({
            program: z.number().optional(),
            serviceType: z.number().optional(),
            recipient: z.number().optional(),
            user: z.string().optional(),
        }),
    })
    .optional()

export type ServiceEntriesFilterParamsType = z.infer<
    typeof ServiceEntriesFilterParams
>

export type UpsertServiceSchemaType = z.infer<typeof UpsertServiceSchema>
