import { z } from 'zod'

import { Prisma } from '@prisma/client'

export const UpsertLocationSchema = z.object({
    id: z.number(),
    address: z.string(),
    address_line2: z.string().nullish(),
    city: z.string(),
    state: z.string(),
    zip: z.string(),
    longitude: z.number(z.instanceof(Prisma.Decimal)),
    latitude: z.number(z.instanceof(Prisma.Decimal)),
})

export type UpsertLocationSchemaType = z.infer<typeof UpsertLocationSchema>
