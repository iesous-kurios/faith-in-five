import { z } from 'zod'

export const UpsertHouseholdSchema = z.object({
    id: z.number(),
    location_id: z.number(),
    name: z.string().nullable(),
    size: z.number().nullable(),
    monthly_income: z.number().nullable(),
    is_unstable: z.boolean(),
})

export type UpsertHouseholdSchemaType = z.infer<typeof UpsertHouseholdSchema>
