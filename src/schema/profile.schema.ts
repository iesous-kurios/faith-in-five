import { z } from 'zod'

export const UpdateProfileSchema = z.object({
    first_name: z
        .string()
        .min(1, 'First name must contain at least 1 character'),
    last_name: z
        .string()
        .min(1, 'Last name must contain at least 1 character')
        .nullable(),
    phone_number: z
        .string()
        .min(10, 'Phone number must contain at least 10 characters')
        .nullable(),
})

export const UpsertProfileSchema = z.object({
    id: z.string(),
    employee_id: z.string().nullable(),
    first_name: z.string().min(1, 'First name is invalid').nullable(),
    last_name: z.string().min(1, 'Last name is invalid').nullable(),
    email: z.string().email(),
    role: z.enum(['user', 'program_manager', 'admin', 'root']),
})

export type UpdateProfileSchemaType = z.infer<typeof UpdateProfileSchema>
export type UpsertProfileSchemaType = z.infer<typeof UpsertProfileSchema>
