import { z } from 'zod'

export const UpsertRecipientSchema = z.object({
    id: z.number(),
    household_id: z.number(),
    first_name: z.string(),
    middle_name: z.string().nullable(),
    last_name: z.string(),
    dob: z.date(),
    is_veteran: z.boolean(),
    has_disability: z.boolean(),
    has_valid_ssi: z.boolean(),
    has_valid_medicare_card: z.boolean(),
})

export type UpsertHouseholdSchemaType = z.infer<typeof UpsertRecipientSchema>
