import React from 'react'

import { User } from '@prisma/client'

import { Controller, SubmitHandler, useForm } from 'react-hook-form'

import { zodResolver } from '@hookform/resolvers/zod'

import {
    UpdateProfileSchema,
    UpdateProfileSchemaType,
} from '@Schema/profile.schema'

import { Button, TextField } from '@mui/material'

import { useUpdateCurrentUser } from 'src/hooks/user.hooks'

const EditProfileForm: React.FC<{ user: User }> = ({ user }) => {
    const { mutate: updateProfile, isLoading } = useUpdateCurrentUser()

    const { control, handleSubmit } = useForm<UpdateProfileSchemaType>({
        resolver: zodResolver(UpdateProfileSchema),
    })

    const onSubmit: SubmitHandler<UpdateProfileSchemaType> = (data) => {
        const { first_name, last_name, phone_number } = data

        // #TODO Better alerts / logging
        updateProfile(
            { first_name, last_name, phone_number },
            { onSuccess: () => alert('Successfully updated profile') }
        )
    }

    return (
        <div className={`bg-white p-8 py-5 rounded-lg shadow-xl`}>
            <h1 className="text-gray-900 font-bold text-3xl py-5 ">Profile</h1>
            <form className="space-y-10" onSubmit={handleSubmit(onSubmit)}>
                <Controller
                    name="first_name"
                    control={control}
                    defaultValue={user.first_name || ''}
                    render={({
                        field: { onChange, value },
                        fieldState: { error },
                    }) => (
                        <TextField
                            label="First Name"
                            variant="filled"
                            value={value}
                            onChange={onChange}
                            error={!!error}
                            helperText={error ? error.message : null}
                            fullWidth
                        />
                    )}
                />
                <Controller
                    name="last_name"
                    control={control}
                    defaultValue={user.last_name || ''}
                    render={({
                        field: { onChange, value },
                        fieldState: { error },
                    }) => (
                        <TextField
                            label="Last Name"
                            variant="filled"
                            value={value}
                            onChange={onChange}
                            error={!!error}
                            helperText={error ? error.message : null}
                            fullWidth
                        />
                    )}
                />

                <Controller
                    name="phone_number"
                    control={control}
                    defaultValue={user.phone_number || ''}
                    render={({
                        field: { onChange, value },
                        fieldState: { error },
                    }) => (
                        <TextField
                            label="Phone Number"
                            variant="filled"
                            value={value}
                            onChange={onChange}
                            error={!!error}
                            helperText={error ? error.message : null}
                            fullWidth
                        />
                    )}
                />

                <hr />

                <TextField
                    label="Email"
                    variant="filled"
                    value={user.email}
                    fullWidth
                    disabled
                />

                <TextField
                    label="Role"
                    variant="filled"
                    value={user.role}
                    fullWidth
                    disabled
                />
                {/* #TODO display list of programs user belongs to*/}
                <Button
                    disabled={isLoading}
                    size={'large'}
                    fullWidth
                    variant={'contained'}
                    type={'submit'}
                >
                    Update
                </Button>
            </form>
        </div>
    )
}

export default EditProfileForm
