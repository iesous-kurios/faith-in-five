import React from 'react'

import { FormControl, Autocomplete, TextField } from '@mui/material'

import { DashboardGetAllResponse } from '@Types/dashboard.types'

const DashboardFilters: React.FC<{ dashboard: DashboardGetAllResponse }> = ({
    dashboard,
}) => {
    return (
        <div className={'flex gap-1 px-1 pt-1 mt-16'}>
            <FormControl fullWidth>
                <Autocomplete
                    disablePortal
                    options={dashboard.programs}
                    getOptionLabel={(opt) => opt.name}
                    renderInput={(params) => (
                        <TextField {...params} label={'Program'} />
                    )}
                />
            </FormControl>
            <FormControl fullWidth>
                <Autocomplete
                    disablePortal
                    options={dashboard.service_types}
                    getOptionLabel={(opt) => opt.name}
                    renderInput={(params) => (
                        <TextField {...params} label={'Service Type'} />
                    )}
                />
            </FormControl>
            <FormControl fullWidth>
                <Autocomplete
                    disablePortal
                    options={dashboard.users}
                    getOptionLabel={(opt) =>
                        `${opt.first_name} ${opt.last_name} (${opt.email})`
                    }
                    renderInput={(params) => (
                        <TextField {...params} label={'Service Provider'} />
                    )}
                />
            </FormControl>
            <FormControl fullWidth>
                <Autocomplete
                    disablePortal
                    options={dashboard.recipients}
                    getOptionLabel={(opt) =>
                        `${opt.first_name} ${opt.last_name}`
                    }
                    renderInput={(params) => (
                        <TextField {...params} label={'Recipient'} />
                    )}
                />
            </FormControl>
        </div>
    )
}

export default DashboardFilters
