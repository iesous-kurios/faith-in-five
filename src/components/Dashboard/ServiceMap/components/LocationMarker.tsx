import React from 'react'

const LocationMarker: React.FC<{
    size: number
    onClick: React.MouseEventHandler<SVGSVGElement>
}> = (props) => {
    const { size = 20, onClick } = props

    return (
        <svg
            height={size}
            viewBox="0 0 24 24"
            className={'stroke-none cursor-pointer'}
            onClick={onClick}
        >
            <circle cx="10" cy="10" r="5" strokeWidth="1" fill={'#5491f5'} />
        </svg>
    )
}

export default LocationMarker
