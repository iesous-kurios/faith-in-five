import React from 'react'

import { LocationWithServicesProvided } from '@Types/location.types'

import Button from '@mui/material/Button'

const LocationPopup: React.FC<{
    location: LocationWithServicesProvided
}> = ({ location }) => {
    return (
        <div className={'text-center'}>
            {location.service_entries.map((entry) => (
                <>
                    <h1 key={entry.id}>{entry.service_type.name}</h1>
                    <h3 key={entry.id}>{entry.location.address}</h3>
                    <h5 key={entry.id}>
                        {entry.recipient.first_name +
                            ' ' +
                            entry.recipient.last_name}
                    </h5>
                    <hr />
                </>
            ))}
            <Button>View Report</Button>
        </div>
    )
}

export default LocationPopup
