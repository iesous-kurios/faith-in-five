import React, { useRef, useState } from 'react'

import { env } from 'src/env/client.mjs'

import ReactMapGL, { Marker, Popup } from 'react-map-gl'

import LocationPopup from './components/LocationPopup'

import LocationMarker from './components/LocationMarker'

import { LocationWithServicesProvided } from '@Types/location.types'

import 'mapbox-gl/dist/mapbox-gl.css'

const ServiceMap: React.FC<{ locations: LocationWithServicesProvided[] }> = ({
    locations,
}) => {
    const { NEXT_PUBLIC_MAPBOX_TOKEN } = env

    const mapRef = useRef(null)

    const [viewport] = useState({
        latitude: 47.658779,
        longitude: -117.426048,
        zoom: 11,
        bearing: 0,
        pitch: 0,
    })

    const [popup, setPopup] = useState<LocationWithServicesProvided | null>(
        null
    )

    const renderLocationMarker = (
        location: LocationWithServicesProvided,
        index: number
    ) => {
        return (
            <Marker
                key={index}
                longitude={location.longitude}
                latitude={location.latitude}
            >
                <LocationMarker size={20} onClick={() => setPopup(location)} />
            </Marker>
        )
    }

    const renderLocationPopup = (
        location: LocationWithServicesProvided | null
    ) => {
        if (location) {
            return (
                <Popup
                    longitude={location.longitude}
                    latitude={location.latitude}
                    anchor="bottom"
                    onClose={() => setPopup(null)}
                    closeOnClick={false}
                >
                    <LocationPopup location={location} />
                </Popup>
            )
        }
    }

    return (
        <ReactMapGL
            ref={mapRef}
            initialViewState={viewport}
            mapStyle="mapbox://styles/mapbox/light-v9"
            style={{ width: '100vw', height: '100vh' }}
            mapboxAccessToken={NEXT_PUBLIC_MAPBOX_TOKEN}
        >
            {locations.map(renderLocationMarker)}
            {renderLocationPopup(popup)}
        </ReactMapGL>
    )
}

export default ServiceMap
