import { Navbar } from '@Components/Partials/Navbar'
import React from 'react'

function Layout({ children }: { children: React.ReactNode }) {
    return (
        <div>
            <Navbar>{children}</Navbar>
        </div>
    )
}

export default Layout
