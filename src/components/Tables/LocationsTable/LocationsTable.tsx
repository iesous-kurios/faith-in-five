import * as React from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/DeleteOutlined'
import SaveIcon from '@mui/icons-material/Save'
import CancelIcon from '@mui/icons-material/Close'

import { User, Location } from '@prisma/client'

import {
    DataGrid,
    GridActionsCellItem,
    GridColumns,
    GridEventListener,
    GridRowId,
    GridRowModes,
    GridRowModesModel,
    GridRowParams,
    GridRowsProp,
    GridToolbarContainer,
    MuiEvent,
} from '@mui/x-data-grid'

import { useDeleteLocation, useUpsertLocation } from 'src/hooks/location.hooks'

import { useEffect } from 'react'

interface EditToolbarProps {
    setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void
    setRowModesModel: (
        newModel: (oldModel: GridRowModesModel) => GridRowModesModel
    ) => void
}

interface Row extends Location {
    isNew: boolean
}

const LocationsTable: React.FC<{
    locations: Location[]
    currentUser: User
}> = ({ locations, currentUser }) => {
    const { mutate: upsertLocation } = useUpsertLocation()

    const { mutate: deleteLocation } = useDeleteLocation()

    const [rows, setRows] = React.useState<Row[]>(locations as Row[])

    const [rowModesModel, setRowModesModel] = React.useState<GridRowModesModel>(
        {}
    )

    const handleRowEditStart = (
        params: GridRowParams,
        event: MuiEvent<React.SyntheticEvent>
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleRowEditStop: GridEventListener<'rowEditStop'> = (
        params,
        event
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleEditClick = (id: GridRowId) => () => {
        if (id == currentUser.id) return // programs can't edit themselves

        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.Edit },
        })
    }

    const handleSaveClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View },
        })
    }

    const handleDeleteClick = (deletedLocationId: GridRowId) => () => {
        deleteLocation({ locationId: deletedLocationId as number })
        setRows(rows.filter((row) => row.id !== deletedLocationId))
    }

    const handleCancelClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View, ignoreModifications: true },
        })

        const editedRow = rows.find((row) => row.id === id)
        if (editedRow!.isNew) {
            setRows(rows.filter((row) => row.id !== id))
        }
    }

    const processRowUpdate = (newLocation: Location) => {
        upsertLocation({ ...newLocation })
        return { ...newLocation, isNew: false }
    }

    const columns: GridColumns = [
        {
            field: 'address',
            headerName: 'Address',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'address_line2',
            headerName: 'Address L2',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'city',
            headerName: 'City',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'state',
            headerName: 'State',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'zip',
            headerName: 'Zip',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'longitude',
            headerName: 'Lon',
            type: 'number',
            editable: true,
            flex: 1,
            valueFormatter: (params) => Number(params.value),
        },
        {
            field: 'latitude',
            headerName: 'Lat',
            type: 'number',
            editable: true,
            flex: 1,
            valueFormatter: (params) => Number(params.value),
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            cellClassName: 'actions',
            getActions: ({ id }) => {
                const isInEditMode =
                    rowModesModel[id]?.mode === GridRowModes.Edit

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            key={id}
                            icon={<SaveIcon />}
                            label="Save"
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            key={id}
                            icon={<CancelIcon />}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ]
                }

                return [
                    <GridActionsCellItem
                        key={id}
                        icon={<EditIcon />}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        key={id}
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                        color="inherit"
                    />,
                ]
            },
        },
    ]

    useEffect(() => {
        setRows(locations as Row[])
    }, [locations])

    return (
        <Box
            sx={{
                height: '70vh',
                width: '100%',
                '& .actions': {
                    color: 'text.secondary',
                },
                '& .textPrimary': {
                    color: 'text.primary',
                },
            }}
        >
            <DataGrid
                rows={rows}
                columns={columns}
                editMode="row"
                rowModesModel={rowModesModel}
                onRowEditStart={handleRowEditStart}
                onRowEditStop={handleRowEditStop}
                // Can't edit/delete yourself or programs of the same role
                processRowUpdate={processRowUpdate}
                disableIgnoreModificationsIfProcessingProps
                components={{
                    Toolbar: EditToolbar,
                }}
                componentsProps={{
                    toolbar: { setRows, setRowModesModel },
                }}
                experimentalFeatures={{ newEditingApi: true }}
            />
        </Box>
    )
}

function EditToolbar(props: EditToolbarProps) {
    const { setRows, setRowModesModel } = props

    const handleClick = () => {
        const id = 10000

        setRows((oldRows) => [
            ...oldRows,
            {
                id,
                latitude: 0,
                longitude: 0,
                last_name: '',
                email: '',
                role: '',
            },
        ])
        setRowModesModel((oldModel) => ({
            ...oldModel,
            [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' },
        }))
    }

    return (
        <GridToolbarContainer>
            <Button
                color="primary"
                startIcon={<AddIcon />}
                onClick={handleClick}
            >
                Add record
            </Button>
        </GridToolbarContainer>
    )
}

export default LocationsTable
