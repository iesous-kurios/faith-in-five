import * as React from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/DeleteOutlined'
import SaveIcon from '@mui/icons-material/Save'
import CancelIcon from '@mui/icons-material/Close'

import { Household, Location, Recipient } from '@prisma/client'

import {
    DataGrid,
    GridActionsCellItem,
    GridColumns,
    GridEventListener,
    GridRenderEditCellParams,
    GridRowId,
    GridRowModes,
    GridRowModesModel,
    GridRowParams,
    GridRowsProp,
    GridToolbarContainer,
    MuiEvent,
    useGridApiContext,
} from '@mui/x-data-grid'

import {
    useDeleteRecipient,
    useUpsertRecipient,
} from 'src/hooks/recipient.hooks'

import { useEffect } from 'react'
import { HouseholdAutocomplete } from '@Components/Tables/Partials/HouseholdAutocomplete/HouseholdAutocomplete'

interface EditToolbarProps {
    setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void
    setRowModesModel: (
        newModel: (oldModel: GridRowModesModel) => GridRowModesModel
    ) => void
}

interface Row extends Recipient {
    isNew: boolean
}

const RecipientsTable: React.FC<{
    recipients: Recipient[]
    households: Household[]
}> = ({ recipients, households }) => {
    const { mutate: upsertRecipient } = useUpsertRecipient()

    const { mutate: deleteRecipient } = useDeleteRecipient()

    const [rows, setRows] = React.useState<Row[]>(recipients as Row[])

    const [rowModesModel, setRowModesModel] = React.useState<GridRowModesModel>(
        {}
    )

    const handleRowEditStart = (
        params: GridRowParams,
        event: MuiEvent<React.SyntheticEvent>
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleRowEditStop: GridEventListener<'rowEditStop'> = (
        params,
        event
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleEditClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.Edit },
        })
    }

    const handleSaveClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View },
        })
    }

    const handleDeleteClick = (deletedRecipientId: GridRowId) => () => {
        deleteRecipient({ recipientId: deletedRecipientId as number })
        setRows(rows.filter((row) => row.id !== deletedRecipientId))
    }

    const handleCancelClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View, ignoreModifications: true },
        })

        const editedRow = rows.find((row) => row.id === id)
        if (editedRow!.isNew) {
            setRows(rows.filter((row) => row.id !== id))
        }
    }

    const processRowUpdate = (newRecipient: Recipient) => {
        upsertRecipient(newRecipient)
        return { ...newRecipient, isNew: false }
    }

    const columns: GridColumns = [
        {
            field: 'first_name',
            headerName: 'First',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'middle_name',
            headerName: 'Middle',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'last_name',
            headerName: 'Last',
            type: 'string',
            editable: true,
            flex: 1,
        },

        {
            field: 'dob',
            headerName: 'Dob',
            type: 'date',
            editable: true,
            flex: 1,
        },

        {
            field: 'household_id',
            headerName: 'Household',
            type: 'number',
            editable: true,
            flex: 1,
            valueGetter: (params) => {
                return params.row.households.name
            },
            renderEditCell: (params) => {
                return (
                    <HouseholdAutocomplete
                        id={params.id}
                        field={params.field}
                        value={params.value}
                        households={households}
                    />
                )
            },
        },
        {
            field: 'is_veteran',
            headerName: 'Veteran',
            type: 'boolean',
            editable: true,
            flex: 1,
        },
        {
            field: 'has_disability',
            headerName: 'Disability',
            type: 'boolean',
            editable: true,
            flex: 1,
        },
        {
            field: 'has_valid_ssi',
            headerName: 'SSI',
            type: 'boolean',
            editable: true,
            flex: 1,
        },
        {
            field: 'has_valid_medicare_card',
            headerName: 'Medicare',
            type: 'boolean',
            editable: true,
            flex: 1,
        },
        {
            field: 'is_active',
            headerName: 'Active',
            type: 'boolean',
            editable: true,
            flex: 1,
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            cellClassName: 'actions',
            getActions: ({ id }) => {
                const isInEditMode =
                    rowModesModel[id]?.mode === GridRowModes.Edit

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            key={id}
                            icon={<SaveIcon />}
                            label="Save"
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            key={id}
                            icon={<CancelIcon />}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ]
                }

                return [
                    <GridActionsCellItem
                        key={id}
                        icon={<EditIcon />}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        key={id}
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                        color="inherit"
                    />,
                ]
            },
        },
    ]

    useEffect(() => {
        setRows(recipients as Row[])
    }, [recipients])

    return (
        <Box
            sx={{
                height: '70vh',
                width: '100%',
                '& .actions': {
                    color: 'text.secondary',
                },
                '& .textPrimary': {
                    color: 'text.primary',
                },
            }}
        >
            <DataGrid
                rows={rows}
                columns={columns}
                editMode="row"
                rowModesModel={rowModesModel}
                onRowEditStart={handleRowEditStart}
                onRowEditStop={handleRowEditStop}
                // Can't edit/delete yourself or programs of the same role
                processRowUpdate={processRowUpdate}
                disableIgnoreModificationsIfProcessingProps
                components={{
                    Toolbar: EditToolbar,
                }}
                componentsProps={{
                    toolbar: { setRows, setRowModesModel },
                }}
                experimentalFeatures={{ newEditingApi: true }}
            />
        </Box>
    )
}

function EditToolbar(props: EditToolbarProps) {
    const { setRows, setRowModesModel } = props

    const handleClick = () => {
        const id = 10000

        setRows((oldRows) => [
            ...oldRows,
            {
                id,
                latitude: 0,
                longitude: 0,
                last_name: '',
                email: '',
                role: '',
            },
        ])
        setRowModesModel((oldModel) => ({
            ...oldModel,
            [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' },
        }))
    }

    return (
        <GridToolbarContainer>
            <Button
                color="primary"
                startIcon={<AddIcon />}
                onClick={handleClick}
            >
                Add record
            </Button>
        </GridToolbarContainer>
    )
}

export default RecipientsTable
