import * as React from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/DeleteOutlined'
import SaveIcon from '@mui/icons-material/Save'
import CancelIcon from '@mui/icons-material/Close'

import { Household, Location } from '@prisma/client'

import {
    DataGrid,
    GridActionsCellItem,
    GridColumns,
    GridEventListener,
    GridRenderEditCellParams,
    GridRowId,
    GridRowModes,
    GridRowModesModel,
    GridRowParams,
    GridRowsProp,
    GridToolbarContainer,
    MuiEvent,
    useGridApiContext,
} from '@mui/x-data-grid'

import {
    useDeleteHousehold,
    useUpsertHousehold,
} from 'src/hooks/household.hooks'

import { FormControl, Autocomplete, TextField } from '@mui/material'

import { useEffect } from 'react'
import { LocationAutocomplete } from '@Components/Tables/Partials/LocationAutocomplete/LocationAutocomplete'

interface EditToolbarProps {
    setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void
    setRowModesModel: (
        newModel: (oldModel: GridRowModesModel) => GridRowModesModel
    ) => void
}

interface Row extends Household {
    isNew: boolean
}

const HouseholdsTable: React.FC<{
    households: Household[]
    locations: Location[]
}> = ({ households, locations }) => {
    const { mutate: upsertHousehold } = useUpsertHousehold()

    const { mutate: deleteHousehold } = useDeleteHousehold()

    const [rows, setRows] = React.useState<Row[]>(households as Row[])

    const [rowModesModel, setRowModesModel] = React.useState<GridRowModesModel>(
        {}
    )

    const handleRowEditStart = (
        params: GridRowParams,
        event: MuiEvent<React.SyntheticEvent>
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleRowEditStop: GridEventListener<'rowEditStop'> = (
        params,
        event
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleEditClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.Edit },
        })
    }

    const handleSaveClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View },
        })
    }

    const handleDeleteClick = (deletedHouseholdId: GridRowId) => () => {
        deleteHousehold({ householdId: deletedHouseholdId as number })
        setRows(rows.filter((row) => row.id !== deletedHouseholdId))
    }

    const handleCancelClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View, ignoreModifications: true },
        })

        const editedRow = rows.find((row) => row.id === id)
        if (editedRow!.isNew) {
            setRows(rows.filter((row) => row.id !== id))
        }
    }

    const processRowUpdate = (newHousehold: Household) => {
        upsertHousehold(newHousehold)
        return { ...newHousehold, isNew: false }
    }

    const columns: GridColumns = [
        {
            field: 'name',
            headerName: 'Name',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'location_id',
            headerName: 'Location',
            type: 'number',
            editable: true,
            flex: 1,
            valueGetter: (params) => {
                return params.row.Location.address
            },
            renderEditCell: (params) => {
                return (
                    <LocationAutocomplete
                        id={params.id}
                        field={params.field}
                        value={params.value}
                        locations={locations}
                    />
                )
            },
        },
        {
            field: 'size',
            headerName: 'Size',
            type: 'number',
            editable: true,
            flex: 1,
        },
        {
            field: 'monthly_income',
            headerName: 'Income',
            type: 'number',
            editable: true,
            flex: 1,
        },
        {
            field: 'is_unstable',
            headerName: 'unstable',
            type: 'boolean',
            editable: true,
            flex: 1,
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            cellClassName: 'actions',
            getActions: ({ id }) => {
                const isInEditMode =
                    rowModesModel[id]?.mode === GridRowModes.Edit

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            key={id}
                            icon={<SaveIcon />}
                            label="Save"
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            key={id}
                            icon={<CancelIcon />}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ]
                }

                return [
                    <GridActionsCellItem
                        key={id}
                        icon={<EditIcon />}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        key={id}
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                        color="inherit"
                    />,
                ]
            },
        },
    ]

    useEffect(() => {
        setRows(households as Row[])
    }, [households])

    return (
        <Box
            sx={{
                height: '70vh',
                width: '100%',
                '& .actions': {
                    color: 'text.secondary',
                },
                '& .textPrimary': {
                    color: 'text.primary',
                },
            }}
        >
            <DataGrid
                rows={rows}
                columns={columns}
                editMode="row"
                rowModesModel={rowModesModel}
                onRowEditStart={handleRowEditStart}
                onRowEditStop={handleRowEditStop}
                // Can't edit/delete yourself or programs of the same role
                processRowUpdate={processRowUpdate}
                disableIgnoreModificationsIfProcessingProps
                components={{
                    Toolbar: EditToolbar,
                }}
                componentsProps={{
                    toolbar: { setRows, setRowModesModel },
                }}
                experimentalFeatures={{ newEditingApi: true }}
            />
        </Box>
    )
}

function EditToolbar(props: EditToolbarProps) {
    const { setRows, setRowModesModel } = props

    const handleClick = () => {
        const id = 10000

        setRows((oldRows) => [
            ...oldRows,
            {
                id,
                latitude: 0,
                longitude: 0,
                last_name: '',
                email: '',
                role: '',
                Location: {
                    address: '',
                },
            },
        ])
        setRowModesModel((oldModel) => ({
            ...oldModel,
            [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' },
        }))
    }

    return (
        <GridToolbarContainer>
            <Button
                color="primary"
                startIcon={<AddIcon />}
                onClick={handleClick}
            >
                Add record
            </Button>
        </GridToolbarContainer>
    )
}

export default HouseholdsTable
