import * as React from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/DeleteOutlined'
import SaveIcon from '@mui/icons-material/Save'
import CancelIcon from '@mui/icons-material/Close'

import { ServiceEntry } from '@prisma/client'

import {
    DataGrid,
    GridActionsCellItem,
    GridColumns,
    GridEventListener,
    GridRowId,
    GridRowModes,
    GridRowModesModel,
    GridRowParams,
    GridRowsProp,
    GridToolbarContainer,
    MuiEvent,
} from '@mui/x-data-grid'

import { useDeleteService, useUpsertService } from 'src/hooks/service.hooks'

import { useEffect } from 'react'

import { useSession } from 'next-auth/react'
import { DashboardGetAllResponse } from '@Types/dashboard.types'
import { LocationAutocomplete } from '@Components/Tables/Partials/LocationAutocomplete/LocationAutocomplete'
import { HouseholdAutocomplete } from '@Components/Tables/Partials/HouseholdAutocomplete/HouseholdAutocomplete'
import { RecipientAutocomplete } from '@Components/Tables/Partials/RecipientAutocomplete/RecipientAutocomplete'
import { ProgramAutocomplete } from '@Components/Tables/Partials/ProgramAutocomplete/ProgramAutocomplete'
import { ServiceTypeAutocomplete } from '@Components/Tables/Partials/ServiceTypeAutocomplete/ServiceTypeAutocomplete'

interface EditToolbarProps {
    setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void
    setRowModesModel: (
        newModel: (oldModel: GridRowModesModel) => GridRowModesModel
    ) => void
}

interface Row extends ServiceEntry {
    isNew: boolean
}

const ServicesTable: React.FC<{
    services: ServiceEntry[]
    dashboard: DashboardGetAllResponse
}> = ({ services, dashboard }) => {
    const session = useSession()

    const { mutate: upsertHousehold } = useUpsertService()

    const { mutate: deleteHousehold } = useDeleteService()

    const [rows, setRows] = React.useState<Row[]>(services as Row[])

    const [rowModesModel, setRowModesModel] = React.useState<GridRowModesModel>(
        {}
    )

    const handleRowEditStart = (
        params: GridRowParams,
        event: MuiEvent<React.SyntheticEvent>
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleRowEditStop: GridEventListener<'rowEditStop'> = (
        params,
        event
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleEditClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.Edit },
        })
    }

    const handleSaveClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View },
        })
    }

    const handleDeleteClick = (deletedServiceId: GridRowId) => () => {
        deleteHousehold({ serviceId: deletedServiceId as number })
        setRows(rows.filter((row) => row.id !== deletedServiceId))
    }

    const handleCancelClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View, ignoreModifications: true },
        })

        const editedRow = rows.find((row) => row.id === id)
        if (editedRow!.isNew) {
            setRows(rows.filter((row) => row.id !== id))
        }
    }

    const processRowUpdate = (newService: ServiceEntry) => {
        // @ts-ignore
        upsertHousehold({ ...newService, user_id: session.data?.user?.id })
        return { ...newService, isNew: false }
    }

    const columns: GridColumns = [
        {
            field: 'recipient_id',
            headerName: 'Recipient',
            type: 'number',
            editable: true,
            flex: 1,
            valueGetter: (params) => {
                return (
                    params.row.recipient.first_name +
                    ' ' +
                    params.row.recipient.last_name
                )
            },
            renderEditCell: (params) => {
                return (
                    <RecipientAutocomplete
                        id={params.id}
                        field={params.field}
                        value={params.value}
                        recipients={dashboard.recipients}
                    />
                )
            },
        },
        {
            field: 'location_id',
            headerName: 'Location',
            type: 'number',
            editable: true,
            flex: 1,
            valueGetter: (params) => {
                return params.row.location.address
            },
            renderEditCell: (params) => {
                return (
                    <LocationAutocomplete
                        id={params.id}
                        field={params.field}
                        value={params.value}
                        locations={dashboard.locations}
                    />
                )
            },
        },
        {
            field: 'household_id',
            headerName: 'Household',
            type: 'number',
            editable: true,
            flex: 1,
            valueGetter: (params) => {
                return params.row.household.name
            },
            renderEditCell: (params) => {
                return (
                    <HouseholdAutocomplete
                        id={params.id}
                        field={params.field}
                        value={params.value}
                        households={dashboard.households}
                    />
                )
            },
        },
        {
            field: 'program_id',
            headerName: 'Program',
            type: 'number',
            editable: true,
            flex: 1,
            valueGetter: (params) => {
                return params.row.program.name
            },
            renderEditCell: (params) => {
                return (
                    <ProgramAutocomplete
                        id={params.id}
                        field={params.field}
                        value={params.value}
                        programs={dashboard.programs}
                    />
                )
            },
        },
        {
            field: 'service_type_id',
            headerName: 'Service Type',
            type: 'number',
            editable: true,
            flex: 1,
            valueGetter: (params) => {
                return params.row.service_type.name
            },
            renderEditCell: (params) => {
                return (
                    <ServiceTypeAutocomplete
                        id={params.id}
                        field={params.field}
                        value={params.value}
                        serviceTypes={dashboard.service_types}
                    />
                )
            },
        },

        {
            field: 'apply_service_to_household',
            headerName: 'Apply Household',
            type: 'boolean',
            editable: true,
            flex: 1,
        },
        {
            field: 'service_date',
            headerName: 'Date',
            type: 'date',
            editable: true,
            flex: 1,
        },
        {
            field: 'service_quantity',
            headerName: 'Quantity',
            type: 'number',
            editable: true,
            flex: 1,
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            flex: 1,
            cellClassName: 'actions',
            getActions: ({ id }) => {
                const isInEditMode =
                    rowModesModel[id]?.mode === GridRowModes.Edit

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            key={id}
                            icon={<SaveIcon />}
                            label="Save"
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            key={id}
                            icon={<CancelIcon />}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ]
                }

                return [
                    <GridActionsCellItem
                        key={id}
                        icon={<EditIcon />}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        key={id}
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                        color="inherit"
                    />,
                ]
            },
        },
    ]

    useEffect(() => {
        setRows(services as Row[])
    }, [services])

    return (
        <Box
            sx={{
                height: '70vh',
                width: '100%',
                '& .actions': {
                    color: 'text.secondary',
                },
                '& .textPrimary': {
                    color: 'text.primary',
                },
            }}
        >
            <DataGrid
                rows={rows}
                columns={columns}
                editMode="row"
                rowModesModel={rowModesModel}
                onRowEditStart={handleRowEditStart}
                onRowEditStop={handleRowEditStop}
                // Can't edit/delete yourself or programs of the same role
                processRowUpdate={processRowUpdate}
                disableIgnoreModificationsIfProcessingProps
                components={{
                    Toolbar: EditToolbar,
                }}
                componentsProps={{
                    toolbar: { setRows, setRowModesModel },
                }}
                experimentalFeatures={{ newEditingApi: true }}
            />
        </Box>
    )
}

function EditToolbar(props: EditToolbarProps) {
    const { setRows, setRowModesModel } = props

    const handleClick = () => {
        const id = 10000

        setRows((oldRows) => [
            ...oldRows,
            {
                id,
                latitude: 0,
                longitude: 0,
                last_name: '',
                email: '',
                role: '',
                location: {
                    address: '',
                },

                household: {
                    name: '',
                },

                recipient: {
                    first_name: '',
                    last_name: '',
                },

                program: {
                    name: '',
                },

                service_type: {
                    name: '',
                },
            },
        ])
        setRowModesModel((oldModel) => ({
            ...oldModel,
            [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' },
        }))
    }

    return (
        <GridToolbarContainer>
            <Button
                color="primary"
                startIcon={<AddIcon />}
                onClick={handleClick}
            >
                Add record
            </Button>
        </GridToolbarContainer>
    )
}

export default ServicesTable
