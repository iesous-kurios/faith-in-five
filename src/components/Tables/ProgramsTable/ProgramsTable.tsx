import * as React from 'react'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/DeleteOutlined'
import SaveIcon from '@mui/icons-material/Save'
import CancelIcon from '@mui/icons-material/Close'

import { Program, User } from '@prisma/client'

import {
    DataGrid,
    GridActionsCellItem,
    GridColumns,
    GridEventListener,
    GridPreProcessEditCellProps,
    GridRowId,
    GridRowModes,
    GridRowModesModel,
    GridRowParams,
    GridRowsProp,
    GridToolbarContainer,
    MuiEvent,
} from '@mui/x-data-grid'
import { faker } from '@faker-js/faker'

import {
    useDeleteProgram,
    useUpsertProgram,
} from '../../../hooks/program.hooks'
import { useEffect } from 'react'
import { styled } from '@mui/material/styles'

interface EditToolbarProps {
    setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void
    setRowModesModel: (
        newModel: (oldModel: GridRowModesModel) => GridRowModesModel
    ) => void
}

interface Row extends Program {
    isNew: boolean
    hasError: boolean
}

const StyledBox = styled(Box)(({ theme }) => ({
    height: '70vh',
    width: '100%',
    '& .MuiDataGrid-cell--editing': {
        backgroundColor: 'rgb(255,215,115, 0.19)',
        color: '#1a3e72',
        '& .MuiInputBase-root': {
            height: '100%',
        },
    },
    '& .Mui-error': {
        backgroundColor: `rgb(126,10,15, ${
            theme.palette.mode === 'dark' ? 0 : 0.1
        })`,
        color: theme.palette.error.main,
    },
}))

const ProgramsTable: React.FC<{ programs: Program[]; currentUser: User }> = ({
    programs,
    currentUser,
}) => {
    const { mutate: upsertProgram } = useUpsertProgram()

    const { mutate: deleteProgram } = useDeleteProgram()

    const [rows, setRows] = React.useState<Row[]>(programs as Row[])

    const [rowModesModel, setRowModesModel] = React.useState<GridRowModesModel>(
        {}
    )

    const handleRowEditStart = (
        params: GridRowParams,
        event: MuiEvent<React.SyntheticEvent>
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleRowEditStop: GridEventListener<'rowEditStop'> = (
        params,
        event
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleEditClick = (id: GridRowId) => () => {
        if (id == currentUser.id) return // programs can't edit themselves

        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.Edit },
        })
    }

    const handleSaveClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View },
        })
    }

    const handleDeleteClick = (deletedProgramId: GridRowId) => () => {
        deleteProgram({ programId: deletedProgramId as number })
        setRows(rows.filter((row) => row.id !== deletedProgramId))
    }

    const handleCancelClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View, ignoreModifications: true },
        })

        const editedRow = rows.find((row) => row.id === id)
        if (editedRow!.isNew) {
            setRows(rows.filter((row) => row.id !== id))
        }
    }

    const processRowUpdate = (newProgram: Program) => {
        upsertProgram(newProgram)

        return { ...newProgram, isNew: false }
    }

    const columns: GridColumns = [
        {
            field: 'name',
            headerName: 'Program',
            type: 'string',
            editable: true,
            flex: 1,
            preProcessEditCellProps: (params: GridPreProcessEditCellProps) => {
                return { ...params.props, error: params.props.value.length < 3 }
            },
        },
        {
            field: 'description',
            headerName: 'Description',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            cellClassName: 'actions',
            getActions: ({ id }) => {
                const isInEditMode =
                    rowModesModel[id]?.mode === GridRowModes.Edit

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            key={id}
                            icon={<SaveIcon />}
                            label="Save"
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            key={id}
                            icon={<CancelIcon />}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ]
                }

                return [
                    <GridActionsCellItem
                        key={id}
                        icon={<EditIcon />}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        key={id}
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                        color="inherit"
                    />,
                ]
            },
        },
    ]

    useEffect(() => {
        setRows(programs as Row[])
    }, [programs])

    return (
        <StyledBox>
            <DataGrid
                rows={rows}
                columns={columns}
                editMode="row"
                rowModesModel={rowModesModel}
                onRowEditStart={handleRowEditStart}
                onRowEditStop={handleRowEditStop}
                processRowUpdate={processRowUpdate}
                components={{
                    Toolbar: EditToolbar,
                }}
                componentsProps={{
                    toolbar: { setRows, setRowModesModel },
                }}
                experimentalFeatures={{ newEditingApi: true }}
            />
        </StyledBox>
    )
}

function EditToolbar(props: EditToolbarProps) {
    const { setRows, setRowModesModel } = props

    const handleClick = () => {
        const id = faker.datatype.number()

        setRows((oldRows) => [
            ...oldRows,
            {
                id,
                employee_id: '',
                first_name: '',
                last_name: '',
                email: '',
                role: '',
            },
        ])
        setRowModesModel((oldModel) => ({
            ...oldModel,
            [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' },
        }))
    }

    return (
        <GridToolbarContainer>
            <Button
                color="primary"
                startIcon={<AddIcon />}
                onClick={handleClick}
            >
                Add record
            </Button>
        </GridToolbarContainer>
    )
}

export default ProgramsTable
