import { User } from '@prisma/client'

const roles = {
    user: [],
    program_manager: ['user'],
    admin: ['user', 'program_manager'],
    root: ['user', 'program_manager', 'admin'],
}

export const renderColumnRoles = (role: User['role']) => {
    return roles[role]
}
