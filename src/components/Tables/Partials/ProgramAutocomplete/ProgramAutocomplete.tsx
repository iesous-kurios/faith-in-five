import { GridRowId, useGridApiContext } from '@mui/x-data-grid'
import { Program } from '@prisma/client'
import { Autocomplete, FormControl, TextField } from '@mui/material'
import * as React from 'react'

export const ProgramAutocomplete: React.FC<{
    id: GridRowId
    field: string
    value: string
    programs: Program[]
}> = ({ id, field, value, programs }) => {
    const apiRef = useGridApiContext()

    const handleValueChange = ({}, program: Program | null) => {
        apiRef.current.setEditCellValue({
            id,
            field,
            value: program?.id,
        })
    }

    return (
        <FormControl fullWidth>
            <Autocomplete
                options={programs}
                onChange={handleValueChange}
                getOptionLabel={(opt) => opt.name}
                defaultValue={programs.find((p) => p.name === value)}
                renderInput={(params) => <TextField {...params} />}
            />
        </FormControl>
    )
}
