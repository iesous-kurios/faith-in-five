import { GridRowId, useGridApiContext } from '@mui/x-data-grid'
import { Recipient } from '@prisma/client'
import { Autocomplete, FormControl, TextField } from '@mui/material'
import * as React from 'react'

export const RecipientAutocomplete: React.FC<{
    id: GridRowId
    field: string
    value: string
    recipients: Recipient[]
}> = ({ id, field, value, recipients }) => {
    const apiRef = useGridApiContext()

    const handleValueChange = ({}, recipient: Recipient | null) => {
        apiRef.current.setEditCellValue({
            id,
            field,
            value: recipient?.id,
        })
    }

    return (
        <FormControl fullWidth>
            <Autocomplete
                options={recipients}
                onChange={handleValueChange}
                defaultValue={recipients.find(
                    (r) => r.first_name + ' ' + r.last_name === value
                )}
                getOptionLabel={(opt) => `${opt.first_name} ${opt.last_name}`}
                renderInput={(params) => <TextField {...params} />}
            />
        </FormControl>
    )
}
