import { GridRowId, useGridApiContext } from '@mui/x-data-grid'

import { Household } from '@prisma/client'
import { Autocomplete, FormControl, TextField } from '@mui/material'
import * as React from 'react'

export const HouseholdAutocomplete: React.FC<{
    id: GridRowId
    field: string
    value: string
    households: Household[]
}> = ({ id, field, value, households }) => {
    const apiRef = useGridApiContext()

    const handleValueChange = ({}, newHousehold: Household | null) => {
        apiRef.current.setEditCellValue({
            id,
            field,
            value: newHousehold?.id,
        })
    }

    return (
        <FormControl fullWidth>
            <Autocomplete
                options={households}
                onChange={handleValueChange}
                getOptionLabel={(opt) => opt.name!}
                defaultValue={households.find((h) => h.name === value)}
                renderInput={(params) => <TextField {...params} />}
            />
        </FormControl>
    )
}
