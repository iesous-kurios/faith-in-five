import { GridRowId, useGridApiContext } from '@mui/x-data-grid'
import { Location } from '@prisma/client'
import { Autocomplete, FormControl, TextField } from '@mui/material'
import * as React from 'react'

// @ts-ignore
export const LocationAutocomplete: React.FC<{
    id: GridRowId
    field: string
    value: string
    locations: Location[]
}> = ({ id, field, value, locations }) => {
    const apiRef = useGridApiContext()

    const handleValueChange = ({}, newLocation: Location | null) => {
        apiRef.current.setEditCellValue({
            id,
            field,
            value: newLocation?.id,
        })
    }

    return (
        <FormControl fullWidth>
            <Autocomplete
                options={locations}
                onChange={handleValueChange}
                getOptionLabel={(opt) => opt.address}
                defaultValue={locations.find((loc) => loc.address === value)}
                renderInput={(params) => <TextField {...params} />}
            />
        </FormControl>
    )
}
