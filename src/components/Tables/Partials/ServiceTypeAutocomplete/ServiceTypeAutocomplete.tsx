import { GridRowId, useGridApiContext } from '@mui/x-data-grid'
import { ServiceType } from '@prisma/client'
import { Autocomplete, FormControl, TextField } from '@mui/material'
import * as React from 'react'

export const ServiceTypeAutocomplete: React.FC<{
    id: GridRowId
    field: string
    value: string
    serviceTypes: ServiceType[]
}> = ({ id, field, value, serviceTypes }) => {
    const apiRef = useGridApiContext()

    const handleValueChange = ({}, service: ServiceType | null) => {
        apiRef.current.setEditCellValue({
            id,
            field,
            value: service?.id,
        })
    }

    return (
        <FormControl fullWidth>
            <Autocomplete
                options={serviceTypes}
                onChange={handleValueChange}
                getOptionLabel={(opt) => opt.name}
                defaultValue={serviceTypes.find((s) => s.name === value)}
                renderInput={(params) => <TextField {...params} />}
            />
        </FormControl>
    )
}
