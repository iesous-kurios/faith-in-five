// TODO Make sure ids aren't getting updated on the server

import React, { useEffect } from 'react'

import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/DeleteOutlined'
import SaveIcon from '@mui/icons-material/Save'
import CancelIcon from '@mui/icons-material/Close'

import { User } from '@prisma/client'

import {
    DataGrid,
    GridActionsCellItem,
    GridColumns,
    GridEventListener,
    GridRowId,
    GridRowModes,
    GridRowModesModel,
    GridRowParams,
    GridRowsProp,
    GridToolbarContainer,
    MuiEvent,
    GridPreProcessEditCellProps,
} from '@mui/x-data-grid'
import { faker } from '@faker-js/faker'

import { renderColumnRoles } from '@Components/Tables/utils/renderColumnRoles'

import { useUpsertUser, useDeleteUser } from '../../../hooks/user.hooks'

interface EditToolbarProps {
    setRows: (newRows: (oldRows: GridRowsProp) => GridRowsProp) => void
    setRowModesModel: (
        newModel: (oldModel: GridRowModesModel) => GridRowModesModel
    ) => void
}

interface Row extends User {
    isNew: boolean
}

const EmployeesTable: React.FC<{ users: User[]; currentUser: User }> = ({
    users,
    currentUser,
}) => {
    const { mutate: createUser } = useUpsertUser()

    const { mutate: deleteUser } = useDeleteUser()

    const [rows, setRows] = React.useState<Row[]>(users as Row[])

    const [rowModesModel, setRowModesModel] = React.useState<GridRowModesModel>(
        {}
    )

    const handleRowEditStart = (
        params: GridRowParams,
        event: MuiEvent<React.SyntheticEvent>
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleRowEditStop: GridEventListener<'rowEditStop'> = (
        params,
        event
    ) => {
        event.defaultMuiPrevented = true
    }

    const handleEditClick = (id: GridRowId) => () => {
        if (id == currentUser.id) return // users can't edit themselves

        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.Edit },
        })
    }

    const handleSaveClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View },
        })
    }

    const handleDeleteClick = (id: GridRowId) => () => {
        if (id == currentUser.id) return // users can't delete themselves

        deleteUser({ userId: id as string })
        setRows(rows.filter((row) => row.id !== id))
    }

    const handleCancelClick = (id: GridRowId) => () => {
        setRowModesModel({
            ...rowModesModel,
            [id]: { mode: GridRowModes.View, ignoreModifications: true },
        })

        const editedRow = rows.find((row) => row.id === id)
        if (editedRow!.isNew) {
            setRows(rows.filter((row) => row.id !== id))
        }
    }

    const processRowUpdate = (newRow: User) => {
        createUser(newRow)

        return { ...newRow, isNew: false }
    }

    const columns: GridColumns = [
        {
            field: 'employee_id',
            headerName: 'EID',
            type: 'string',
            editable: true,
            flex: 1,
            preProcessEditCellProps: (params: GridPreProcessEditCellProps) => {
                const hasError = !params.props.value.length
                return { ...params.props, error: hasError }
            },
        },
        {
            field: 'first_name',
            headerName: 'First name',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'last_name',
            headerName: 'Last name',
            type: 'string',
            editable: true,
            flex: 1,
        },
        {
            field: 'email',
            headerName: 'Email',
            type: 'string',
            editable: true,
            flex: 1,
            valueParser: (email: string) => email.toLowerCase(),
        },
        {
            field: 'role',
            headerName: 'role',
            type: 'singleSelect',
            valueOptions: renderColumnRoles('root'),
            editable: true,
            flex: 1,
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            cellClassName: 'actions',
            getActions: ({ id }) => {
                const isInEditMode =
                    rowModesModel[id]?.mode === GridRowModes.Edit

                if (isInEditMode) {
                    return [
                        <GridActionsCellItem
                            key={id}
                            icon={<SaveIcon />}
                            label="Save"
                            onClick={handleSaveClick(id)}
                        />,
                        <GridActionsCellItem
                            key={id}
                            icon={<CancelIcon />}
                            label="Cancel"
                            className="textPrimary"
                            onClick={handleCancelClick(id)}
                            color="inherit"
                        />,
                    ]
                }

                return [
                    <GridActionsCellItem
                        key={id}
                        icon={<EditIcon />}
                        label="Edit"
                        className="textPrimary"
                        onClick={handleEditClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        key={id}
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={handleDeleteClick(id)}
                        color="inherit"
                    />,
                ]
            },
        },
    ]

    useEffect(() => {
        setRows(users as Row[])
    }, [users])

    return (
        <Box
            sx={{
                height: '70vh',
                width: '100%',
                '& .actions': {
                    color: 'text.secondary',
                },
                '& .textPrimary': {
                    color: 'text.primary',
                },
            }}
        >
            <DataGrid
                rows={rows}
                columns={columns}
                editMode="row"
                rowModesModel={rowModesModel}
                onRowEditStart={handleRowEditStart}
                onRowEditStop={handleRowEditStop}
                // Can't edit/delete yourself or users of the same role
                isCellEditable={(params) => params.row.id !== currentUser.id}
                processRowUpdate={processRowUpdate}
                disableIgnoreModificationsIfProcessingProps
                components={{
                    Toolbar: EditToolbar,
                }}
                componentsProps={{
                    toolbar: { setRows, setRowModesModel },
                }}
                experimentalFeatures={{ newEditingApi: true }}
            />
        </Box>
    )
}

function EditToolbar(props: EditToolbarProps) {
    const { setRows, setRowModesModel } = props

    const handleClick = () => {
        const id = faker.datatype.uuid()

        setRows((oldRows) => [
            ...oldRows,
            {
                id,
                employee_id: '',
                first_name: '',
                last_name: '',
                email: '',
                role: '',
            },
        ])
        setRowModesModel((oldModel) => ({
            ...oldModel,
            [id]: { mode: GridRowModes.Edit, fieldToFocus: 'name' },
        }))
    }

    return (
        <GridToolbarContainer>
            <Button
                color="primary"
                startIcon={<AddIcon />}
                onClick={handleClick}
            >
                Add record
            </Button>
        </GridToolbarContainer>
    )
}

export default EmployeesTable
