import { describe, expect } from '@jest/globals'

describe('jest configuration', () => {
    it('runs the test without errors', () => {
        expect(1 + 1).toBe(2)
    })
})
