# Family Promise Service Tracker

Family Promise helps local communities coordinate their compassion to address the root causes of family homelessness. They tap existing local resources to empower families towards economic stability. Families come to them in crisis; they help them rebuild their lives with new skills and ongoing support. They address the issue holistically, providing prevention services before families reach crisis, shelter and case management when they become homeless, and stabilization programs once they have secured housing to ensure they remain independent.

Family Promise needs a way to track and visualize the services they provide external to the shelter to gain actionable insights.

Our goal is to build a generalizable monitoring and evaluation (M&E) platform that meets Family Promise's needs, with an eye toward additional potential use cases that would be useful for many other organizations.

## Tech Stack

-   Client/Server - [React/Next](https://nextjs.org/)
-   Database - [Postgres](https://www.postgresql.org/)
-   ORM - [Prisma](https://www.prisma.io/)
-   API - [tRPC](https://trpc.io/)
-   State - [React-Query](https://tanstack.com/query/v4/?from=reactQueryV3&original=https://react-query-v3.tanstack.com/)
-   Forms - [React-Hook-Form](https://react-hook-form.com/)
-   Styling - [Tailwindcss](https://tailwindcss.com/)
-   UI library - [Material](https://mui.com/)
-   Input validation - [Zod](https://zod.dev/)
-   Authentication - [NextAuth](https://next-auth.js.org/)

## Development Server

In order to set up your development environment you will need to download some prerequisites:

-   `docker`
-   `docker-compose`
-   `node 16.17.x`
-   `npm` setup and configured on your machine.

Below are instructions for setting up your development environment.
You may also use [n](https://www.npmjs.com/package/n) to manage your node versions by running:

```bash
sudo npm i -g n
```

```bash
sudo n 16
```

### Install Prereqs

-   Docker Desktop https://www.docker.com/products/docker-desktop/
-   Node 16 (LTS) https://nodejs.org/en/download/

```bash
docker run hello-world
```

```bash
node -v
```

You should see the following (or a version slightly higher)

```bash
v16.17.1
```

### Setup environment

```bash
# Install dependencies, create environment variables and run database migrations/seeds
npm run first-time-setup
```

Once this completes, the following command will start up a development server on your local machine:

```bash
# Starts the docker containers for PostgreSQL, MailHog and runs Next.js with hot-reload support
npm run start:dev
```

After those servers boot, you can run migrations on your database

```bash
npm run prisma:migrate
```

### Create An Account

Before you can actually see anything in the ServiceTracker UI, you'll have to create an account:

1. Go to http://localhost:3100
2. Click on `Sign in`, and enter an email
3. Open your inbox at http://localhost:8080 to verify your account

### Change user role

The ServiceTracker behaves differently depending on the role of the user. We're going to change it
to admin by connecting to postgres directly

1. Run `npm run prisma:studio` to open the database explorer
2. Click on `User`
3. Double-click your role and select admin from the dropdown
4. Save your changes and refresh the browser

### Commonly used URL's

```bash
# The front-end React UI
http://localhost:3100
# The backend Next.js server
http://localhost:3100/api
# MailHog inbox
http://localhost:8080
```

## Making changes to the database

1. Edit the database schema inside `prisma/schema.prisma`
2. Run `npm run prisma:format` to check for errors
3. Run `npm run prisma:migrate`

## Build

Run `npm run next:build` to build the project for production. The build artifacts will be stored in the `.next` directory.

## Running Tests

1. Run `npm run start:dev` to build all the necessary docker containers, database migrations, and start the test server
2. Once the development server is started, you can run `npm run test` to execute the entire test suite of unit and end-to-end tests.

### End-to-End Tests

End-to-end tests are provided via [Cypress](https://www.cypress.io/), and are located in the `./cypress` directory. You can run just the Cypress tests by running `npm run test:cypress`, and can run individual tests by executing `npm run test:cypress -- --spec <path-to-spec>`.

### Unit Tests

Unit tests are provided via [Jest](https://jestjs.io/), and are located in the `src/__tests__/` directory. You can run just the Jest tests by running `npm run test:jest`, and can run individual tests by executing `npm run test:jest -- <path-to-spec>`.

## Useful resources

-   [Next in 100s](https://www.youtube.com/watch?v=Sklc_fQBmcs)
-   [Prisma in 100s](https://www.youtube.com/watch?v=rLRIB6AF2Dg)
-   [React-Query in 100s](https://www.youtube.com/watch?v=novnyCaa7To)
-   [Tailwind in 100s](https://www.youtube.com/watch?v=mr15Xzb1Ook)
-   [TypeScript in 100s](https://www.youtube.com/watch?v=zQnBQ4tB3ZA)
-   [Docker in 100s](https://www.youtube.com/watch?v=Gjnup-PuquQ)
-   [Build a Blog With the T3 Stack](https://www.youtube.com/watch?v=syEWlxVFUrY)
-   [Build a Live Chat Application with the T3 Stack](https://www.youtube.com/watch?v=dXRRY37MPuk)
