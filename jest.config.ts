/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/configuration
 */
import type { Config } from 'jest'

// Client and server jest tests are now in separate projects.
// Each folder contains a jest.config.js that inherits from here.

const config: Config = {
    preset: 'ts-jest',

    // See https://github.com/swagger-api/swagger-ui/issues/7944
    transformIgnorePatterns: [
        '/node_modules/(?!(swagger-client|react-syntax-highlighter)/)',
    ],
    globals: {
        'ts-jest': {
            isolatedModules: true,
        },
    },
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
    testPathIgnorePatterns: ['cypress/', 'dist/', 'test-helpers.ts', 'lib/'],
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    workerIdleMemoryLimit: '200MB',
    watchPathIgnorePatterns: ['node_modules', '.git'],
    coverageThreshold: {
        global: {
            statements: 90,
            branches: 75,
            functions: 80,
            lines: 90,
        },
    },
    coveragePathIgnorePatterns: [
        'server/(.*).module.ts',
        'lib/',
        'config.ts',
        '.*.html',
        'server/utility/.*',
    ],
    coverageReporters: ['json', 'text', 'lcovonly', 'clover'],
    collectCoverage: true,
}

export default config
