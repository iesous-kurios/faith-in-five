describe('Feature: Home Page', () => {
    it('should allow any user to visit the home page', () => {
        cy.visit('/')
        cy.url().should('include', '/')
    })
})
