/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{js,ts,jsx,tsx}'],
    theme: {
        keyframes: {
            wiggle: {
                '0%, 90%': { transform: 'translateX(-2px)' },
                '50%': { transform: 'translateX(2px)' },
                '100%': { transform: 'translateX(0px)' },
            },
        },
        extend: {
            animation: {
                wiggle: 'wiggle 0.1s ease-in-out',
            },
        },
    },
    plugins: [],
}
