generator client {
  provider      = "prisma-client-js"
  binaryTargets = ["native", "rhel-openssl-1.0.x"]
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model Account {
  id                String  @id @default(cuid())
  userId            String
  type              String
  provider          String
  providerAccountId String
  refresh_token     String? //@db.Text
  access_token      String? //@db.Text
  expires_at        Int?
  token_type        String?
  scope             String?
  id_token          String? //@db.Text
  session_state     String?
  user              User    @relation(fields: [userId], references: [id], onDelete: Cascade)

  @@unique([provider, providerAccountId])
}

model Session {
  id           String   @id @default(cuid())
  sessionToken String   @unique
  userId       String
  expires      DateTime
  user         User     @relation(fields: [userId], references: [id], onDelete: Cascade)
}

model User {
  id              String         @id @default(cuid())
  employee_id     String?
  first_name      String?
  last_name       String?
  name            String?
  email           String         @unique
  emailVerified   DateTime?
  phone_number    String?
  image           String?
  accounts        Account[]
  sessions        Session[]
  role            Role           @default(user)
  service_entries ServiceEntry[]
}

model VerificationToken {
  identifier String
  token      String   @unique
  expires    DateTime

  @@unique([identifier, token])
}

model Location {
  id              Int            @id @default(autoincrement())
  address         String         @db.VarChar(255)
  address_line2   String?        @db.VarChar(255)
  city            String         @db.VarChar(255)
  state           String         @db.VarChar(255)
  zip             String         @db.VarChar(255)
  longitude       Float
  latitude        Float
  created_at      DateTime       @default(now()) @db.Timestamptz(6)
  updated_at      DateTime       @default(now()) @db.Timestamptz(6)
  households      Household[]
  service_entries ServiceEntry[]
}

model Household {
  id             Int            @id @default(autoincrement())
  location_id    Int
  name           String?        @db.VarChar(255)
  size           Int?
  monthly_income Float?
  is_unstable    Boolean        @default(false)
  created_at     DateTime       @default(now()) @db.Timestamptz(6)
  updated_at     DateTime       @default(now()) @db.Timestamptz(6)
  Location       Location?      @relation(fields: [location_id], references: [id], onDelete: Cascade, map: "Household_location_id_foreign")
  recipients     Recipient[]
  ServiceEntry   ServiceEntry[]
}

model Recipient {
  id                      Int            @id @default(autoincrement())
  household_id            Int
  first_name              String         @db.VarChar(255)
  middle_name             String?        @db.VarChar(255)
  last_name               String         @db.VarChar(255)
  is_active               Boolean        @default(true)
  dob                     DateTime       @db.Date
  is_veteran              Boolean        @default(false)
  has_disability          Boolean        @default(false)
  has_valid_ssi           Boolean        @default(false)
  has_valid_medicare_card Boolean        @default(false)
  recipient_notes         String?
  created_at              DateTime       @default(now()) @db.Timestamptz(6)
  updated_at              DateTime       @default(now()) @db.Timestamptz(6)
  households              Household      @relation(fields: [household_id], references: [id], onUpdate: Restrict, map: "recipients_household_id_foreign")
  ServiceEntry            ServiceEntry[]
}

model Program {
  id           Int            @id @default(autoincrement())
  name         String         @db.VarChar(255)
  description  String?
  is_active    Boolean        @default(true)
  created_at   DateTime       @default(now()) @db.Timestamptz(6)
  updated_at   DateTime       @default(now()) @db.Timestamptz(6)
  ServiceEntry ServiceEntry[]
}

model ServiceType {
  id           Int            @id @default(autoincrement())
  name         String         @db.VarChar(255)
  description  String?
  is_active    Boolean        @default(true)
  entry_model  Json?
  created_at   DateTime       @default(now()) @db.Timestamptz(6)
  updated_at   DateTime       @default(now()) @db.Timestamptz(6)
  ServiceEntry ServiceEntry[]
}

model ServiceEntry {
  id                         Int         @id @default(autoincrement())
  user_id                    String      @db.VarChar(255)
  recipient_id               Int
  location_id                Int
  household_id               Int
  program_id                 Int
  service_type_id            Int
  apply_service_to_household Boolean     @default(false)
  service_date               DateTime    @db.Date
  service_quantity           Int?
  service_entry_notes        String?
  created_at                 DateTime    @default(now()) @db.Timestamptz(6)
  updated_at                 DateTime    @default(now()) @db.Timestamptz(6)
  location                   Location    @relation(fields: [location_id], references: [id], onUpdate: Restrict, map: "service_entries_location_id_foreign")
  user                       User        @relation(fields: [user_id], references: [id], onUpdate: Restrict, map: "service_entries_primary_user_id_foreign")
  household                  Household   @relation(fields: [household_id], references: [id], onUpdate: Restrict, map: "service_entries_primary_houehold_id_foreign")
  service_type               ServiceType @relation(fields: [service_type_id], references: [id], onUpdate: Restrict, map: "service_entries_primary_service_type_id_foreign")
  recipient                  Recipient   @relation(fields: [recipient_id], references: [id], onUpdate: Restrict, map: "service_entries_primary_recipient_id_foreign")
  program                    Program     @relation(fields: [program_id], references: [id], onUpdate: Restrict, map: "service_entries_primary_program_id_foreign")
}

// ** CUSTOM TYPES **
enum Role {
  user
  program_manager
  admin
  root
}
