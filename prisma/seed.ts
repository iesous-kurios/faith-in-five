import { PrismaClient, User } from '@prisma/client'

import { faker } from '@faker-js/faker'

const prisma = new PrismaClient()

async function main() {
    await prisma.serviceEntry.deleteMany()
    await prisma.serviceType.deleteMany()
    await prisma.program.deleteMany()
    await prisma.account.deleteMany()
    await prisma.session.deleteMany()
    await prisma.user.deleteMany()
    await prisma.recipient.deleteMany()
    await prisma.household.deleteMany()
    await prisma.location.deleteMany()

    await prisma.serviceType.createMany({
        data: service_types,
    })

    await prisma.program.createMany({
        data: programs,
    })

    await prisma.user.createMany({
        data: users,
    })

    locations.forEach(
        async (loc) => await prisma.location.create({ data: loc })
    )
}
main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })

const programs = [
    { id: 1, name: 'Prevention', description: 'rent description' },
]

const locations = [
    {
        id: 1,
        address: '7010 n colton st apt J205',
        city: 'Spokane',
        state: 'Washington',
        zip: '99208',
        latitude: 47.721399,
        longitude: -117.4097697,
        households: {
            create: {
                name: faker.name.fullName(),
                size: 3,
                monthly_income: 300,
                recipients: {
                    create: {
                        first_name: faker.name.firstName(),
                        middle_name: faker.name.middleName(),
                        last_name: faker.name.lastName(),
                        dob: faker.date.birthdate(),
                    },
                },
            },
        },
    },
    {
        id: 2,
        address: '1935 N . Holy Names Court D208',
        city: 'Spokane',
        state: 'Washington',
        zip: '99224',
        latitude: 47.6757514,
        longitude: -117.4595423,
        households: {
            create: {
                name: faker.name.fullName(),
                size: 3,
                monthly_income: 300,
                recipients: {
                    create: {
                        first_name: faker.name.firstName(),
                        middle_name: faker.name.middleName(),
                        last_name: faker.name.lastName(),
                        dob: faker.date.birthdate(),
                    },
                },
            },
        },
    },
    {
        id: 3,
        address: '220 S Nelson St',
        city: 'Spokane',
        state: 'Washington',
        zip: '99202',
        latitude: 47.654528,
        longitude: -117.3716446,
        households: {
            create: {
                name: faker.name.fullName(),
                size: 3,
                monthly_income: 300,
                recipients: {
                    create: {
                        first_name: faker.name.firstName(),
                        middle_name: faker.name.middleName(),
                        last_name: faker.name.lastName(),
                        dob: faker.date.birthdate(),
                    },
                },
            },
        },
    },
    {
        id: 4,
        address: '428 e dalton ave',
        city: 'Spokane',
        state: 'Washington',
        zip: '99207',
        latitude: 47.6867604,
        longitude: -117.403608,
        households: {
            create: {
                name: faker.name.fullName(),
                size: 3,
                monthly_income: 300,
                recipients: {
                    create: {
                        first_name: faker.name.firstName(),
                        middle_name: faker.name.middleName(),
                        last_name: faker.name.lastName(),
                        dob: faker.date.birthdate(),
                    },
                },
            },
        },
    },
    {
        id: 5,
        address: '4909 E upriver DR',
        city: 'Spokane',
        state: 'Washington',
        zip: '99217',
        latitude: 47.6812337,
        longitude: -117.3419548,
        households: {
            create: {
                name: faker.name.fullName(),
                size: 3,
                monthly_income: 300,
                recipients: {
                    create: {
                        first_name: faker.name.firstName(),
                        middle_name: faker.name.middleName(),
                        last_name: faker.name.lastName(),
                        dob: faker.date.birthdate(),
                    },
                },
            },
        },
    },
]

const users = [
    {
        id: '1l8m6m7rb0006naun4r1dr1vr',
        email: 'admin@gmail.com',
        role: 'admin' as User['role'],
    },
    {
        id: '2l8m6m7rb0006naun4r1dr1vr',
        email: 'root@gmail.com',
        role: 'root' as User['role'],
    },
    {
        id: '333m6m7rb0006naun4r1dr1vr',
        email: 'pm@gmail.com',
        role: 'program_manager' as User['role'],
    },
    {
        id: '4l8m6m7rb0006naun4r1dr1vr',
        email: 'user@gmail.com',
        role: 'user' as User['role'],
    },
]

const service_types = [
    {
        id: 2,
        name: 'Showers',
        description:
            'Self cleaning and care opportunities for currently homeless individuals',
        is_active: true,
        entry_model: {},
    },
    {
        id: 3,
        name: 'Laundry',
        description:
            'Personal clothing cleaning opportunities provided to currently homeless individuals',
        is_active: true,
        entry_model: {},
    },
    {
        id: 4,
        name: 'Case Management',
        description: '',
        is_active: true,
        entry_model: {},
    },
    {
        id: 5,
        name: 'Food Boxes',
        description:
            'Food boxes provided to individuals and/or households to solidify stability among current program recipients',
        is_active: true,
        entry_model: {
            custom: [
                {
                    name_of_field: 'Food for what amount of persons?',
                    description: 'How many people need food?',
                    data_type: 'quantity',
                    required: true,
                    display_field_as: 'entry',
                    options: null,
                },
                {
                    name_of_field: 'Food Allergies?',
                    description:
                        'Are there any allergies we need to be aware of?',
                    data_type: 'text',
                    required: true,
                    display_field_as: 'checkbox',
                    options: [
                        'None',
                        'Shellfish',
                        'Peanuts',
                        'Gluten',
                        'Milk',
                        'Eggs',
                        'Other',
                    ],
                },
                {
                    name_of_field: 'If other allergy, what allergy?',
                    description: 'Please write the allergy',
                    data_type: 'text',
                    required: false,
                    display_field_as: 'entry',
                    options: null,
                },
            ],
        },
    },
    {
        id: 6,
        name: 'Gas card',
        description:
            'Prepaid gas cards provided to eligible program recipients',
        is_active: true,
        entry_model: {
            custom: [], // establish gas card type?
        },
    },
    {
        id: 7,
        name: 'Food card',
        description:
            'Prepaid food cards provided to eligible program recipients',
        is_active: true,
        entry_model: {
            custom: [], // establish food card type? Safeway? Hannafords? Subway sandwiches?
        },
    },
    {
        id: 8,
        name: 'Security Deposit',
        description:
            'Provide recipient with funding to help secure rental apartment',
        entry_model: {
            custom: [
                {
                    name_of_field: 'What was the security deposit for?',
                    description: 'Choose which category fits best',
                    data_type: 'text',
                    required: true,
                    display_field_as: 'dropdown',
                    options: [
                        'Apartment',
                        'Vehicle',
                        'Electric Utility',
                        'Water Utility',
                        'Other Utility',
                    ],
                },
                {
                    name_of_field: 'Refundable security deposit?',
                    description: 'Is this a refundable security deposit?',
                    data_type: 'boolean',
                    required: true,
                    display_field_as: 'dropdown',
                    options: [true, false],
                },
                {
                    name_of_field: 'Security deposit due date',
                    description: 'When does this have to be paid by?',
                    data_type: 'date',
                    required: true,
                    display_field_as: 'entry',
                    options: null,
                },
            ],
        },
    },
    {
        id: 9,
        name: 'Bus Tokens',
        description: 'Provide recipient with a set amount of bus tokens',
        entry_model: {
            custom: [
                {
                    name_of_field: 'Bus Service Provider',
                    description: 'Which bus service are the tokens for?',
                    data_type: 'text',
                    required: true,
                    display_field_as: 'dropdown',
                    options: [
                        'Spokane Transit Authority',
                        'InterCity Transit',
                        'FlixBus',
                        'Greyhound',
                    ],
                },
            ],
        },
    },
    {
        id: 10,
        name: 'Bus Passes',
        is_active: true,
        description: 'Provide recipient with a bus pass',
        entry_model: {
            custom: [
                {
                    name_of_field: 'Bus Service Provider',
                    description: 'Which bus service are the tokens for?',
                    data_type: 'text',
                    required: true,
                    display_field_as: 'dropdown',
                    options: [
                        'Spokane Transit Authority',
                        'InterCity Transit',
                        'FlixBus',
                        'Greyhound',
                    ],
                },
                {
                    name_of_field: 'Bus Pass Start Date',
                    description: 'When will the pass start working',
                    data_type: 'date',
                    required: true,
                    display_field_as: 'entry',
                    options: null,
                },
                {
                    name_of_field: 'Bus Pass End Date',
                    description: 'When will the pass stop working',
                    data_type: 'date',
                    required: true,
                    display_field_as: 'entry',
                    options: null,
                },
            ],
        },
    },
    {
        id: 11,
        name: 'Rental Assistance',
        description:
            'Provide recipient experiencing financial hardship with funds to help pay current month of rent',
        is_active: true,
        entry_model: {
            custom: [
                {
                    name_of_field: 'Date rent is due',
                    description: 'When is the rent payment due?',
                    data_type: 'date',
                    required: true,
                    display_field_as: 'entry',
                    options: null,
                },
                {
                    name_of_field:
                        'Is rental assistance expected to be required again next month?',
                    description:
                        'Based on currently known information, what is your assessment?',
                    data_type: 'text',
                    required: true,
                    display_field_as: 'dropdown',
                    options: ['Yes', 'No', 'Unclear'],
                },
            ],
        },
    },
    {
        id: 12,
        name: 'Food Assistance', // for homeless (Listed as Food/Meals in comments from stakeholders)
        description:
            'Provide recipient experiencing food insecurity with food delivery',
        entry_model: {
            custom: [
                {
                    name_of_field: 'Food for what amount of persons?',
                    description: 'How many people need food?',
                    data_type: 'quantity',
                    required: true,
                    display_field_as: 'entry',
                    options: null,
                },
                {
                    name_of_field: 'Food Allergies?',
                    description:
                        'Are there any allergies we need to be aware of?',
                    data_type: 'text',
                    required: true,
                    display_field_as: 'checkbox',
                    options: [
                        'None',
                        'Shellfish',
                        'Peanuts',
                        'Gluten',
                        'Milk',
                        'Eggs',
                        'Other',
                    ],
                },
                {
                    name_of_field: 'If other allergy, what allergy?',
                    description: 'Please write the allergy',
                    data_type: 'text',
                    required: false,
                    display_field_as: 'entry',
                    options: null,
                },
            ],
        },
    },
    {
        id: 13,
        name: 'Mental Health Counseling',
        description:
            'Provide recipient with mental health counseling session at the Mental Health Clinic',
        entry_model: {
            custom: [
                {
                    name_of_field: 'Visible emotional state of client',
                    description:
                        "How does the client's emotional state appear to you?",
                    data_type: 'text',
                    required: true,
                    display_field_as: 'dropdown',
                    options: [
                        'Normal',
                        'Excited',
                        'Agitated',
                        'Apathetic',
                        'Distressed',
                        'Depressed',
                        'Other',
                    ],
                },
                {
                    name_of_field: 'Is follow up be necessary?',
                    description: 'Does the client need to be seen again?',
                    data_type: 'boolean',
                    required: true,
                    display_field_as: 'dropdown',
                    options: [true, false],
                },
            ],
        },
    },
    {
        id: 14,
        name: 'Life Skills Classes',
        description:
            'Provide recipient with classes to learn skills required to prevent return to homelessness',
        entry_model: {
            custom: [
                {
                    name_of_field: 'Which life skills were covered?',
                    description:
                        "Select which life skills were covered in today's class",
                    data_type: 'text',
                    required: true,
                    display_field_as: 'checkbox',
                    options: [
                        'Budgeting',
                        'Time Management',
                        'Interpersonal Skills',
                        'Meal Planning',
                        'Resume Prep',
                        'Filing Taxes',
                        'Other',
                    ],
                },
                {
                    name_of_field:
                        'If other, what skill was covered, why, and how?',
                    description:
                        'Please detail the skill that was covered, why they requested that skill, and how you covered teaching it',
                    data_type: 'text',
                    required: false,
                    display_field_as: 'entry',
                    options: null,
                },
                {
                    name_of_field: 'Will follow up sessions be required?',
                    description:
                        'Does the recipient require additional sessions to master the skills covered?',
                    data_type: 'boolean',
                    required: true,
                    display_field_as: 'dropdown',
                    options: [true, false],
                },
            ],
        },
    },
]
