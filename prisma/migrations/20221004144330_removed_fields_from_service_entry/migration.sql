/*
  Warnings:

  - You are about to drop the column `service_duration` on the `ServiceEntry` table. All the data in the column will be lost.
  - You are about to drop the column `service_entry_data` on the `ServiceEntry` table. All the data in the column will be lost.
  - You are about to drop the column `service_time` on the `ServiceEntry` table. All the data in the column will be lost.
  - You are about to drop the column `service_value` on the `ServiceEntry` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "ServiceEntry" DROP COLUMN "service_duration",
DROP COLUMN "service_entry_data",
DROP COLUMN "service_time",
DROP COLUMN "service_value";
