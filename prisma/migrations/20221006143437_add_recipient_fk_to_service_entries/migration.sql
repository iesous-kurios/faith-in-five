-- AddForeignKey
ALTER TABLE "ServiceEntry" ADD CONSTRAINT "service_entries_primary_recipient_id_foreign" FOREIGN KEY ("recipient_id") REFERENCES "Recipient"("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
