-- AddForeignKey
ALTER TABLE "ServiceEntry" ADD CONSTRAINT "service_entries_primary_program_id_foreign" FOREIGN KEY ("program_id") REFERENCES "Program"("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
