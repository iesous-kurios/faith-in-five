import fs from 'fs'

const {
    DATABASE_URL,
    NEXTAUTH_SECRET,
    NEXTAUTH_URL,
    GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET,
    FACEBOOK_CLIENT_ID,
    FACEBOOK_CLIENT_SECRET,
    EMAIL_SERVER,
    EMAIL_FROM,
    NEXT_PUBLIC_MAPBOX_TOKEN
} = process.env;

fs.readFile('.env.sample', 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }

    data = data.replace(/DATABASE_URL=.*/g, 'DATABASE_URL='                         + DATABASE_URL);
    data = data.replace(/NEXTAUTH_URL=.*/g, 'NEXTAUTH_URL='                         + NEXTAUTH_URL);
    data = data.replace(/NEXTAUTH_SECRET=.*/g, 'NEXTAUTH_SECRET='                   + NEXTAUTH_SECRET);
    data = data.replace(/GOOGLE_CLIENT_ID=.*/g, 'GOOGLE_CLIENT_ID='                 + GOOGLE_CLIENT_ID);
    data = data.replace(/GOOGLE_CLIENT_SECRET=.*/g, 'GOOGLE_CLIENT_SECRET='         + GOOGLE_CLIENT_SECRET);
    data = data.replace(/FACEBOOK_CLIENT_ID=.*/g, 'FACEBOOK_CLIENT_ID='             + FACEBOOK_CLIENT_ID);
    data = data.replace(/FACEBOOK_CLIENT_SECRET=.*/g, 'FACEBOOK_CLIENT_SECRET='     + FACEBOOK_CLIENT_SECRET);
    data = data.replace(/EMAIL_SERVER=.*/g, 'EMAIL_SERVER='                         + EMAIL_SERVER);
    data = data.replace(/EMAIL_FROM=.*/g, 'EMAIL_FROM='                             + EMAIL_FROM);
    data = data.replace(/NEXT_PUBLIC_MAPBOX_TOKEN=.*/g, 'NEXT_PUBLIC_MAPBOX_TOKEN=' + NEXT_PUBLIC_MAPBOX_TOKEN);

    fs.writeFile('.env', data, 'utf8', function (err) {
        if (err) return console.log(err);
    });
});
