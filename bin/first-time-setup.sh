# Setup environment variables
cp .env.sample .env

# Install deps
npm ci

# Fetch docker images and run them in the background
npm run docker:background

echo "Waiting for postgres to initialize, this will take a minute"

sleep 60

echo "Success"
